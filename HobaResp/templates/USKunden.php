<?php
/*
 * Templagte US Kunden
 * Timeline Desing ... Test... l�uft .. .fast
 * Schema.org - ContactPoints fehlen noch.
 */
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/default.css" rel="stylesheet">
<link href="www/cssAddon/timeline.css" rel="stylesheet">
<link href="www/cssAddon/uskunden.css" rel="stylesheet">
<link href="www/cssAddon/gmaps.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-09-19">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&#246;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="U.S.Customers -  Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">

<!-- Twitter -->
<meta name="twitter:title"
	content="U.S.Customers - Autohaus am Stadtpark Holderbaum GmbH">
<meta name="twitter:url" content="www.holderbaum.de">

<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>U.S.Customers - Autohaus am Stadtpark Holderbaum GmbH -
	Volkswagen Service mitten in Kaiserslautern</title>

</head>

<body>
	<!-- Navigatipn -->     
	<?php include 'static/navbar_content.php'; ?>
	<section id="USKunden" itemscope
		itemtype="https://schema.org/LocalBusiness">
		<div class="container">
			<!-- UEberschriftzeile -->
			<div class="row">
				<div class="well text-center">
					<h2>U.S. Customers</h2>
					<h3 class="text-muted">for your convenience</h3>
					<p class="text-muted">Vat accepted - Visa accepted - Mastercard
						accepted - Car Exchange costumers are welcome</p>
				</div>
			</div>

			<!-- Timeline Zeile -->
			<div class="row">
				<div class="col-lg-12">
					<ul class="timeline">
						<li>
							<!-- Servicebox -->
							<div class="timeline-image shadow">
								<img class="img-circle img-responsive"
									src="www/gfx/us/service.jpg" alt="Retro Bild Service">
							</div>
							<div class="timeline-panel">
								<div class="well shadow">
									<div class="timeline-heading">
										<h4>Service</h4>
									</div>
									<div class="timeline-body">
										<p>Your Volkswagen is like a piece of home and with our
											Volkswagen service you will have a competent partner at any
											time on your side. We garanty you best quality, top trained
											staff and mobility.</p>
									<?php shophours('service', $this->data);?>	
                                </div>
								</div>
							</div>
						</li>

						<li class="timeline-inverted">
							<!-- Partsbox -->
							<div class="timeline-image shadow">
								<img class="img-circle img-responsive"
									src="www/gfx/us/parts.jpg" alt="Retro Bild Teile">
							</div>
							<div class="timeline-panel">
								<div class="well shadow">
									<div class="timeline-heading">
										<h4>Parts</h4>
									</div>
									<div class="timeline-body">
										<p>No matter if your Volkswagen needs new break pads or a new
											exhaust system, original quality is the best quality.
											Volkswagen spare parts have been developed together with the
											car, that is why they are perfectly tuned to each individual
											modell.</p>
									<?php shophours('teile', $this->data); ?>	
						 		</div>
								</div>
							</div>
						</li>

						<li>
							<!-- Salesbox -->
							<div class="timeline-image shadow">
								<img class="img-circle img-responsive"
									src="www/gfx/us/salesman.jpg" alt="Retro Bild Verkauf">
							</div>
							<div class="timeline-panel">
								<div class="well shadow">
									<div class="timeline-heading">
										<h4>Sales</h4>
									</div>
									<div class="timeline-body">
										<p>On our Webpage you will find our current list of our used
											and employee cars. If we don't have the proper car for you in
											stock our Seller will find it for you. Feel free to contact
											our Seller to get more information.</p>
										<?php shophours('verkauf', $this->data); ?>
								</div>
								</div>
							</div>
						</li>

						<li class="timeline-inverted">
							<!-- Carsbox -->
							<div class="timeline-image shadow">
								<img class="img-circle img-responsive"
									src="www/gfx/us/sales.jpg" alt="Retro Bild Teile">
							</div>
							<div class="timeline-panel">
								<div class="well shadow">
									<div class="timeline-heading">
										<h4>
											our cars <a href="index.php?view=list"><span
												class="glyphicon glyphicon-th-list"></span> </a>
										</h4>
									</div>
									<div class="timeline-body">
										<!-- Carouselwrapper -->
										<div id="cars" class="carousel slide shadow abstand"
											data-ride="carousel">
											<div class="carousel-inner" role="listbox">
	                				 	<?php
																							$count = 1;
																							foreach ( $this->data ['cars'] ['cars'] as $key => $car ) {
																								foreach ( $car ['bilder'] as $bilder ) {
																									if ($count == 1) {
																										echo '<div class="item active"><a href="index.php?view=ad&ad=' . $key . '">';
																									} else {
																										echo '<div class="item"><a href="index.php?view=ad&ad=' . $key . '">';
																									}
																									echo '<img class="img-responsive" src="' . $bilder ['M'] . '" alt="' . $car ['make_local_desc'] . ' ' . $car ['model_desc'] . '">';
																									echo '<div><strong>' . $car ['preis'] . '&euro;</strong><br>' . $car ['make_local_desc'] . ' ' . $car ['model_desc'] . '</div></a>';
																									echo '</div>';
																								}
																								$count ++;
																							}
																							?>
				                		</div>
											<!-- Listbox -->
										</div>
										<!-- Wrapper -->
									</div>
									<!-- TimelineBody -->
								</div>
							</div>
						</li>
					</ul>
				</div>
				<!-- Spalten -->
			</div>
			<!-- Row -->
		</div>
		<!-- Container -->
	</section>
	<section id="googlemaps">
		<!-- MapContainer -->
		<div class="container">
			<div class="mapcaption" itemscope
				itemtype="http://schema.org/PostalAddress">
				<h4>
					Address : <a itemprop="url"
						href="https://maps.google.com/maps?ll=49.439154,7.763803&z=19&t=m&hl=de&gl=US&mapclient=apiv3&cid=5583030116073235760">
						<span itemprop="name">Autohaus am Stadtpark Holderbaum GmbH</span>,
						<span itemprop="streetAddress">Pirmasenser Str. 57</span>, <span
						itemprop="postalCode">67655 </span> <span
						itemprop="addressLocality">Kaiserslautern</span>
					</a>
				</h4>
			</div>
			<div id="map" class="map"></div>
		</div>
	</section>

	<!-- Footer -->
	<?php include 'static/foot_content.php'; ?>
	
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript">
    $(document).ready(function() {
    			$('#cars').carousel();
    });        
	</script>
	<script src="www/js/gmaps.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?callback=getmap&key=AIzaSyBvwAxYFGKDZ-TPFKjq4yRuoHwR04Fx6Ck"></script>

</body>
</html>
<?php
function shophours($shop, $data) {
	echo '<h4>Shop hours:</h4>';
	$key = key ( $data [$shop] ['zeiten'] );
	$zeiten = $data [$shop] ['zeiten'] [$key]->times;
	foreach ( $zeiten as $zeit )
		if ($zeit->lang == 'en')
			echo '<data itemprop="openingHours" value="' . $zeit->datetime . '">' . $zeit->days . ':' . $zeit->time . '</data><br>';
	$key = key ( $data [$shop] ['kontakt'] );
	$contact = $data [$shop] ['kontakt'] [$key];
	echo '<br><a class="btn btn-primary" href="index.php?view=call&id=' . $contact->id . '"><span class="glyphicon glyphicon-envelope"></span> E-Mail</a> ';
	echo '<a class="btn btn-primary" href="tel:' . rawurlencode ( $contact->phone ) . ' "><span class="glyphicon glyphicon-earphone"></span><span itemprop="telephone">' . $contact->phone . '</span></a>';
}
?>	                			

