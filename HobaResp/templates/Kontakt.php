<?php
/*
 * Templagte Kontakte Kunden
 */
?>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/default.css" rel="stylesheet">
<link href="www/cssAddon/Kontakt.css" rel="stylesheet">
<link href="www/cssAddon/gmaps.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-09-19">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&ouml;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="Kontakte -  Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">

<!-- Twitter -->
<meta name="twitter:title"
	content="Kontakte - Autohaus am Stadtpark Holderbaum GmbH">
<meta name="twitter:url" content="www.holderbaum.de">

<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>Kontakte - Autohaus am Stadtpark Holderbaum GmbH - Volkswagen
	Service mitten in Kaiserslautern</title>

</head>

<body>

	<!-- Navigatipn -->     
	<?php include 'static/navbar_content.php'; ?>
	<div class="container" itemscope
		itemtype="https://schema.org/LocalBusiness">

		<section id="Service">
			<div class="row">
				<div class="well shadow">
					<div class="h2box">
						<!-- H2 -->
						<h2>
							<span>Service</span>
						</h2>
						<img src="www/gfx/kontakte/bg/se.jpg" alt="Bannerbild Service">
					</div>
					<!-- /H2 -->
					<div class="shophours">
						<!-- Shop -->
						<div class="row">
							<div class="col-sm-4">
                   				 <?php shophours('service', $this->data)?>
                   			</div>
							<div class="col-sm-8">
								<p>Original Teil und Zubehoer. Fuer die richtigen Teile fuer sie
									zu finden benoetigen neben der Erstzulassung auch die Fahrzeug
									Ident-Nummer.</p>
							</div>
						</div>
					</div>
					<!-- Shop -->
                    <?php employes('service', $this->data)?>
                </div>
			</div>
		</section>

		<section id="Verkauf">
			<div class="row">
				<div class="well shadow">
					<div class="h2box">
						<h2>
							<span> Verkauf </span>
						</h2>
						<img src="www/gfx/kontakte/bg/vk.jpg" alt="Bannerbild Verkauf">
					</div>
					<div class="shophours">
						<div class="row">
							<div class="col-sm-4">
                    <?php shophours('verkauf', $this->data)?>
                    </div>
							<div class="col-sm-8">
								<p>Original Teil und Zubehoer. Fuer die richtigen Teile fuer sie
									zu finden benoetigen neben der Erstzulassung auch die Fahrzeug
									Ident-Nummer.</p>
							</div>
						</div>
					</div>
                    <?php employes('verkauf', $this->data)?>
                </div>
			</div>
		</section>

		<section id="Teile">
			<div class="row">
				<div class="well shadow">
					<div class="h2box">
						<h2>
							<span> Teile und Zubeh&ouml;r </span>
						</h2>
						<img src="www/gfx/kontakte/bg/lg.jpg" alt="Bannerbild Teile">
					</div>
					<div class="shophours">
						<div class="row">
							<div class="col-sm-4">
                  	  			<?php shophours('teile', $this->data)?>
                    		</div>
							<div class="col-sm-8">
								<p>Original Teil und Zubehoer. Fuer die richtigen Teile fuer sie
									zu finden benoetigen neben der Erstzulassung auch die Fahrzeug
									Ident-Nummer.</p>
							</div>
						</div>
					</div>
                    <?php employes('teile', $this->data)?>
                    </div>
			</div>

		</section>
	</div>
	<section id="googlemaps">
		<div class="container">
			<div class="mapcaption" itemscope
				itemtype="http://schema.org/PostalAddress">
				<h4>
					Anschrift : <a itemprop="url"
						href="https://maps.google.com/maps?ll=49.439154,7.763803&z=19&t=m&hl=de&gl=US&mapclient=apiv3&cid=5583030116073235760">
						<span itemprop="name">Autohaus am Stadtpark Holderbaum GmbH</span>,
						<span itemprop="streetAddress">Pirmasenser Str. 57</span>, <span
						itemprop="postalCode">67655 </span> <span
						itemprop="addressLocality">Kaiserslautern</span>
					</a>
				</h4>
			</div>
			<div id="map" class="map"></div>
		</div>
	</section>

	<!-- Footer -->
	<?php include 'static/foot_content.php'; ?>
<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="www/js/gmaps.js"></script>
	<script
		src="https://maps.googleapis.com/maps/api/js?callback=getmap&key=AIzaSyBvwAxYFGKDZ-TPFKjq4yRuoHwR04Fx6Ck"></script>
</body>
</html>
<?php
function shophours($shop, $data) {
	echo '<h4>&Ouml;ffnungszeiten:</h4>';
	$key = key ( $data [$shop] ['zeiten'] );
	$zeiten = $data [$shop] ['zeiten'] [$key]->times;
	foreach ( $zeiten as $zeit )
		if ($zeit->lang == 'de')
			echo '<data itemprop="openingHours" value="' . $zeit->datetime . '">' . $zeit->days . ':' . $zeit->time . '</data><br>';
	$key = key ( $data [$shop] ['kontakt'] );
	$contact = $data [$shop] ['kontakt'] [$key];
	echo '<br><a class="btn btn-primary" href="index.php?view=call&id=' . $contact->id . '"><span class="glyphicon glyphicon-envelope"></span> E-Mail</a> ';
	echo '<a class="btn btn-primary" href="tel:' . rawurlencode ( $contact->phone ) . ' "><span class="glyphicon glyphicon-earphone"></span><span itemprop="telephone">' . $contact->phone . '</span></a>';
}
function employes($shop, $data) {
	$spalten = 3;
	$counter = 1;
	
	foreach ( $data [$shop] ['employes'] as $employe ) {
		if ($counter == 1) {
			echo '<div class="row">';
		}
		echo '<div class="col-sm-4">';
		echo '<div class="contactholder">';
		echo '<img class="img-responsive img-circle" src="' . $employe->pic . '" alt="' . $employe->name . $employe->desc . '">';
		
		echo '<div>';
		echo '<h4>' . $employe->name . '</h4>';
		echo '<p>' . $employe->desc . '</p>';
		echo '<a class="btn btn-primary" href="index.php?view=call&id=' . $employe->id . '"><span class="glyphicon glyphicon-envelope"></span>&nbsp; E-Mail</a>&nbsp;&nbsp;';
		echo '<a class="btn btn-primary" href="tel:' . rawurlencode ( $employe->phone ) . ' "><span class="glyphicon glyphicon-earphone"></span><span itemprop="telephone">&nbsp;' . $employe->phone . '</span></a>';
		echo '</div>';
		echo '</div>';
		echo '</div>';
		if ($counter == $spalten) {
			echo '</div>';
			$counter = 0;
		}
		$counter ++;
	}
	if ($counter < $spalten && $counter > 1) {
		echo '</div>';
	}
}
?>	                			

