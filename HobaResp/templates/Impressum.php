<?php
/*
 * Templagte f�r das Impressum / Datenschutz.
 * gf = Gesch�ftsf�hrer, ds = Datenschutz kontakte.
 *
 */
?>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/default.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-06-21">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&#246;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="Impressum Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">

<!-- Twitter -->
<meta name="twitter:title"
	content="Impressum Autohaus am Stadtpark Holderbaum GmbH">
<meta name="twitter:url" content="www.holderbaum.de">

<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>Impressum Autohaus am Stadtpark Holderbaum GmbH - Volkswagen
	Service mitten in Kaiserslautern</title>
</head>

<body>
	<!-- Navigatipn -->     
	<?php include 'static/navbar_content.php'; ?>
	 <div class="container">
		<div class="row">
			<div class="well">
				<h1>Impressum</h1>
				<div class="panel-group">
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Herausgeber</h3>
						</div>
						<div class="panel-body">
							Autohaus am Stadtpark Holderbaum GmbH<br> Pirmasenser Str. 57<br>
							67655 Kaiserslautern<br>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Gesch&auml;ftsf&uuml;hrer</h3>
						</div>
						<div class="panel-body">
        		    	<?php
															foreach ( $this->data ['gf'] as $gf ) {
																echo $gf->name . '<br>';
																
																echo '<a class="btn btn-primary" href="index.php?view=call&id=' . $gf->id . '"><span class="glyphicon glyphicon-envelope"></span> E-Mail senden</a> ';
																echo '<a class="btn btn-primary" href="tel:' . rawurlencode ( $gf->phone ) . ' "><span class="glyphicon glyphicon-earphone"></span>' . $gf->phone . '</a>';
																echo '<br>';
															}
															?>
        		    	</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Registergericht</h3>
						</div>
						<div class="panel-body">Amstgericht Kaiserslautern HRB:3507</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>USt-ID</h3>
						</div>
						<div class="panel-body">DE204733515</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Versicherungsvermittlerregister</h3>
						</div>
						<div class="panel-body">
							D-X4XT-F8DR9-56<br> Gebundener Versicherungsvetreter nach
							&sect;34d Abs. 4Gew0 <br>
							<a href="http://www.vermittlerregister.info"><span
								class="glyphicon glyphicon-link"></span>www.vermittlerregister.info
								DIHK</a>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Angaben nach &sect;3 DL-InfoV - Schiedsstelle</h3>
						</div>
						<div class="panel-body">
							Schiedsstelle des Kfz-Gewerbes Kaiserslautern<br> Mannheimer Str.
							132<br> 67657 Kaiserslautern<br> <br> Weitere Informationen zu
							den KFZ-Schiedsstellen, insbesondere zu Verfahren und
							Voraussetzungen zum Zugang k&ouml;nnen auch der Internetadresse <a
								href="http://www.kfz-schiedsstelle.de"><span
								class="glyphicon glyphicon-link"></span>www.kfz-schiedsstelle.de</a>
							entnommen werden.
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Haftung f&uuml;r Links + Urheberrecht</h3>
						</div>
						<div class="panel-body">
							<h4>Haftung f&uuml;r Links</h4>
							Unser Angebot enth&auml;lt Links zu externen Webseiten Dritter,
							auf deren Inhalte wir keinen Einfluss haben. Deshalb k&ouml;nnen
							wir f&uuml;r diese fremden Inhalte auch keine Gew&auml;hr
							&uuml;bernehmen. F&uuml;r die Inhalte der verlinkten Seiten ist
							stets der jeweilige Anbieter oder Betreiber der Seiten
							verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der
							Verlinkung auf m&ouml;gliche Rechtsverst&ouml;sse
							&uuml;berpr&uuml;ft. Rechtswidrige Inhalte waren zum Zeitpunkt
							der Verlinkung nicht erkennbar. Eine permanente inhaltliche
							Kontrolle der verlinkten Seiten ist jedoch ohne konkrete
							Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei
							Bekanntwerden von Rechtsverletzungen werden wir derartige Links
							umgehend entfernen.
							<h4>Urheberrecht</h4>
							Die durch die Seitenbetreiber erstellten Inhalte und Werke auf
							diesen Seiten unterliegen dem deutschen Urheberrecht. Die
							Vervielf&auml;ltigung, Bearbeitung, Verbreitung und jede Art der
							Verwertung ausserhalb der Grenzen des Urheberrechtes
							bed&uuml;rfen der schriftlichen Zustimmung des jeweiligen Autors
							bzw. Erstellers. Downloads und Kopien dieser Seite sind nur
							f&uuml;r den privaten, nicht kommerziellen Gebrauch gestattet.
							Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt
							wurden, werden die Urheberrechte Dritter beachtet. Insbesondere
							werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie
							trotzdem auf eine Urheberrechtsverletzung aufmerksam werden,
							bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von
							Rechtsverletzungen werden wir derartige Inhalte umgehend
							entfernen.
						</div>
					</div>




				</div>
				<!-- Panel-Group -->
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php include 'static/foot_content.php'; ?>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>