<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>
<link href="bootstrap/icss/icss.css" rel="stylesheet">

<!-- Facebook and Twitter integration -->
<meta property="og:title" content="Gebrauchtfahrzeuge" />
<meta property="og:image" content="" />
<meta property="og:url" content="www.holderbaum.de" />
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH" />
<meta property="og:description"
	content="Unser aktueller Fahrzeugbestand" />
<meta name="twitter:title" content="Gebrauchtfahrzeuge" />
<meta name="twitter:image" content="" />
<meta name="twitter:url" content="www.holderbaum.de" />
<meta name="twitter:card" content="" />

<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>Autohaus am Stadtpark Holderbaum GmbH - Service mitten in
	Kaiserslautern!</title>
<!-- lokale Styles -->
<style>
.bs-callout-warning {
	border-right-color: #f0ad4e;
	border-left-color: #f0ad4e;
	border-right-width: 3px;
	border-left-width: 3px;
}

.shadow {
	box-shadow: 2px 2px 3px #888;
}

<!--
Zoom Bilder -->.img-hover img {
	-webkit-transition: all .6s ease; /* Safari and Chrome */
	-moz-transition: all .6s ease; /* Firefox */
	transition: all .6s ease;
}

.img-hover img:hover {
	-webkit-backface-visibility: hidden;
	backface-visibility: hidden;
	-webkit-transform: translateZ(0) scale(1.20); /* Safari and Chrome */
	-moz-transform: scale(1.20); /* Firefox */
	transform: translatZ(0) scale(1.20);
}

<!--
Augensymbol auf Bild -->.ln-thumb-box {
	display: inline-block;
	position: relative;
	overflow: hidden;
}

.ln-thumb-box-overlay {
	display: none;
}

.ln-thumb-box a:hover .ln-thumb-box-overlay {
	display: inline;
	text-align: center;
	position: absolute;
	color: #fff;
	width: 100%;
	height: 100%;
	text-shadow: 0 1px 2px rgba(0, 0, 0, .6);
}

.ln-thumb-box-overlay span {
	position: relative;
	top: 50%;
	-webkit-transform: translateY(-50%);
	-ms-transform: translateY(-50%);
	transform: translateY(-50%);
}

#bgimg {
	position: fixed;
	z-index: -1;
}
</style>
</head>

<body>
	<img src="www/b14.jpg" id="bgimg">

	<nav id="mainNav" class="navbar navbar-default navbar-fixed-top shadow">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand page-scroll" href="#page-top"><img
					class="img-responsive" src="www/gfx/default/logo32.png"></a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li><a class="page-scroll" href="#gutachten">Service</a></li>
					<li><a class="page-scroll" href="#services">Verkauf</a></li>
					<li><a class="page-scroll" href="#services">Zubehör und
							Ersatzteile</a></li>
					<li><a class="page-scroll" href="#contact">Kontakt</a></li>
					<li><a class="page-scroll" href="#impressum">Impressum</a></li>
				</ul>
			</div>
			<!-- /.navbar-collapse -->
		</div>
		<!-- /.container-fluid -->
	</nav>

	<header>
		<div class="header-content">
			<div class="header-content-inner">
				<h1>Kfz-Sachverständiger Th.Becker</h1>
				<hr>
				<p>Datenschutz Wir nehmen den Schutz Ihrer persönlichen Daten sehr
					ernst und halten uns strikt an die Regeln der Datenschutzgesetze.
					Die nachfolgende Erklärung gibt Ihnen einen Überblick darüber,
					wie wir diesen Schutz gewährleisten und welche Art von Daten zu
					welchen Zwecken erhoben werden. Anonyme Datenerhebung In der Regel
					können Sie unsere besuchen, ohne dass wir persönliche Daten von
					Ihnen benötigen. Erhoben und speichert werden Informatonen die Ihr
					Browser an uns übermittelt. Dies sind: Browsertyp/ -version
					Verwendetes Betriebssystem Referer URL Datum und Uhrzeit der
					Serveranfrage Abgerufene Seite Diese Informationen werden zu
					statistischen Zwecken ausgewertet. Sie bleiben als Nutzer hierbei
					anonym.Darüber hinausgehende personenbezogene Angaben wie Ihr
					Name, Ihre Anschrift, Telefonnummer oder E-Mail-Adresse werden
					nicht erfasst, es sei denn, Sie machen diese Angaben freiwillig,
					z.B. im Rahmen einer Informationsanfrage. Automatisch erfasste,
					nicht personenbezogene Daten; aktive Inhalte Beim Zugriff auf
					unsere Webseiten, werden automatisch erzeugte Informationen
					gesammelt, die nicht einer bestimmten Person zugeordnet werden
					können (z. B. über einen Internetbrowser oder ein Betriebssystem;
					Domainnamen von Webseiten, von denen aus über einen Weblink ein
					Zugriff erfolgt; Anzahl der Besucher durchschnittliche Verweilzeit;
					aufgerufene Seiten). Wir verwenden diese Informationen, um den
					Auftritt unserer Webseite ständig zu verbessern, zu aktualisieren
					und somit ihre Attraktivität zu erhöhen. Wenn Sie über einen
					Link unsere Seite verlassen und so auf fremde Seiten gelangen, kann
					es sein, dass auch von Adressaten der angeklickten Zielseite
					Cookies gesetzt werden. Für diese Cookies sind wir rechtlich nicht
					verantwortlich. Bei der. („Google“). Google Analytics verwendet
					sog. „Cookies“, Textdateien, die auf Ihrem Computer gespeichert
					werden und die eine Analyse der Benutzung der Website durch Sie
					ermöglichen. Die durch den Cookie erzeugten Informationen über
					Ihre Benutzung dieser Website werden in der Regel an einen Server
					von Google in den USA übertragen und dort gespeichert. Im Falle
					der Aktivierung der IP-Anonymisierung auf d</p>
				<a href="#gutachten" class="btn btn-primary btn-xl page-scroll">Warum
					ein Gutachten?</a>
			</div>
		</div>
	</header>
	<section>
		<p>Datenschutz Wir nehmen den Schutz Ihrer persönlichen Daten sehr
			ernst und halten uns strikt an die Regeln der Datenschutzgesetze. Die
			nachfolgende Erklärung gibt Ihnen einen Überblick darüber, wie wir
			diesen Schutz gewährleisten und welche Art von Daten zu welchen
			Zwecken erhoben werden. Anonyme Datenerhebung In der Regel können
			Sie unsere besuchen, ohne dass wir persönliche Daten von Ihnen
			benötigen. Erhoben und speichert werden Informatonen die Ihr Browser
			an uns übermittelt. Dies sind: Browsertyp/ -version Verwendetes
			Betriebssystem Referer URL Datum und Uhrzeit der Serveranfrage
			Abgerufene Seite Diese Informationen werden zu statistischen Zwecken
			ausgewertet. Sie bleiben als Nutzer hierbei anonym.Darüber
			hinausgehende personenbezogene Angaben wie Ihr Name, Ihre Anschrift,
			Telefonnummer oder E-Mail-Adresse werden nicht erfasst, es sei denn,
			Sie machen diese Angaben freiwillig, z.B. im Rahmen einer
			Informationsanfrage. Automatisch erfasste, nicht personenbezogene
			Daten; aktive Inhalte Beim Zugriff auf unsere Webseiten, werden
			automatisch erzeugte Informationen gesammelt, die nicht einer
			bestimmten Person zugeordnet werden können (z. B. über einen
			Internetbrowser oder ein Betriebssystem; Domainnamen von Webseiten,
			von denen aus über einen Weblink ein Zugriff erfolgt; Anzahl der
			Besucher durchschnittliche Verweilzeit; aufgerufene Seiten). Wir
			verwenden diese Informationen, um den Auftritt unserer Webseite
			ständig zu verbessern, zu aktualisieren und somit ihre
			Attraktivität zu erhöhen. Wenn Sie über einen Link unsere Seite
			verlassen und so auf fremde Seiten gelangen, kann es sein, dass auch
			von Adressaten der angeklickten Zielseite Cookies gesetzt werden.
			Für diese Cookies sind wir rechtlich nicht verantwortlich. Bei der.
			(„Google“). Google Analytics verwendet sog. „Cookies“,
			Textdateien, die auf Ihrem Computer gespeichert werden und die eine
			Analyse der Benutzung der Website durch Sie ermöglichen. Die durch
			den Cookie erzeugten Informationen über Ihre Benutzung dieser
			Website werden in der Regel an einen Server von Google in den USA
			übertragen und dort gespeichert. Im Falle der Aktivierung der
			IP-Anonymisierung auf d</p>

	</section>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="www/js/scaleBG.js"></script>
</body>
</html>
