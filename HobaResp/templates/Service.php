<?php
/*
 * Templagte Kontakte Kunden
 */
?>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/Portfolio.css" rel="stylesheet">
<link href="www/cssAddon/gmaps.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-09-19">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&ouml;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="Service -  Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">

<!-- Twitter -->
<meta name="twitter:title"
	content="Service - Autohaus am Stadtpark Holderbaum GmbH">
<meta name="twitter:url" content="www.holderbaum.de">

<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>Service - Autohaus am Stadtpark Holderbaum GmbH - Volkswagen
	Service mitten in Kaiserslautern</title>

</head>

<body>

	<!-- Navigatipn -->     
	<?php include 'static/navbar_content.php'; ?>

 <header>
     <div class="container">
        <div class="intro-text">
            <div class="intro-lead-in">Volkswagen Service<br>mitten in</div>
            <div class="intro-heading">Kaiserslautern</div>
            <a href="#portfolio" class="page-scroll fab"><span class="glyphicon glyphicon-chevron-down"></span></a>
        </div>
    </div>
    <video poster="www/gfx/Service/sectionBG.jpg" id="bgvid" playsinline autoplay muted loop>
        <source src="www/film.webm" type="video/webm">
        <source src="www/film.mp4" type="video/mp4">
     </video>
</header>

<section id="portfolio" class="bg_service">
	<div class="container">
        <div class="row">
                <div class="col-lg-12 textcenter">
                    <h2>Unsere Serviceleistungen</h2>
                </div>
		</div>
            
            <div class="row">
			<div class="col-sm-4 portfolio-item">
				<a href="#ServiceModal" class="portfolio-link" data-toggle="modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-plus fa-3x"></i>
						</div>
					</div>
					<img src="www/gfx/Service/bg1.jpg" class="img-responsive" alt="Service Bild">
				</a>
				<div class="portfolio-caption">
					<h4>Service</h4>
					<p>Wartung + Inspektion + Dialogannahme</p>
				</div>
			</div>
					
			<div class="col-sm-4 portfolio-item">
				<a href="#NotdienstModal" class="portfolio-link" data-toggle="modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-plus fa-3x"></i>
						</div>
					</div>
					<img src="www/gfx/Service/bg2.jpg" class="img-responsive" alt="Notdient Bild">
				</a>
				<div class="portfolio-caption">
					<h4>24h Notdienst</h4>
					<p>365 Tage im Jahr</p>
				</div>
			</div>
					
			<div class="col-sm-4 portfolio-item">
				<a href="#Ersatzwagenmodal" class="portfolio-link" data-toggle="modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-plus fa-3x"></i>
						</div>
					</div>
					<img src="www/gfx/Service/bg3.jpg" class="img-responsive" alt="Ersatzwagen Bild">
				</a>
				<div class="portfolio-caption">
					<h4>Ersatzwagen</h4>
					<p>damit Sie immer mobil bleiben</p>
				</div>
			</div>
					
			<div class="col-sm-4 portfolio-item">
				<a href="#HolundBringModal" class="portfolio-link" data-toggle="modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-plus fa-3x"></i>
						</div>
					</div>
					<img src="www/gfx/Service/bg4.jpg" class="img-responsive" alt="Hol und Bring Bild">
				</a>
				<div class="portfolio-caption">
					<h4>Hol-& Bringservice</h4>
					<p>Wir bringen Ihr Auto zum Service</p>
				</div>
			</div>

			<div class="col-sm-4 portfolio-item">
				<a href="#hauptuntersuchung" class="portfolio-link" data-toggle="modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-plus fa-3x"></i>
						</div>
					</div>
					<img src="www/gfx/Service/bg5.jpg" class="img-responsive" alt="HU Bild">
				</a>
				<div class="portfolio-caption">
					<h4>Hauptuntersuchung HU</h4>
					<p>Plakettenservice</p>
				</div>
			</div>

			<div class="col-sm-4 portfolio-item">
				<a href="#reifeneinlagernmodal" class="portfolio-link" data-toggle="modal">
					<div class="portfolio-hover">
						<div class="portfolio-hover-content">
							<i class="fa fa-plus fa-3x"></i>
						</div>
					</div>
					<img src="www/gfx/Service/bg6.jpg" class="img-responsive" alt="Reifen Einlagerung Bild">
				</a>
				<div class="portfolio-caption">
					<h4>Reifen- und Räderwechsel</h4>
					<p>Einlagerungsservice</p>
				</div>
                </div>
		</div><!-- row -->
	
     <div class="row">
                <div class="col-lg-12 c">
                    <img src="www/gfx/Service/bg6.jpg">
                </div>
		</div>
    </div> <!-- Container -->
</section>

<section id="finanz">
  <h2>Finanzdienstleistungen rund um Ihr Auto</h2>
	<div class="container">
		<div class="row">
			 <div class="row text-center">
                <div class="col-md-3">
                    <img src="www/gfx/sales/finanzierung.jpg" class="img-responsive img-circle center-block salesicon">
                    <h4>Finanzierung</h4>
                    <p class="text-muted">
                        Sie suchen sich einfach Ihr Auto aus, entscheiden, ob und wie viel Sie anzahlen und legen die gewünschte Laufzeit fest. So erwerben Sie Eigentum zu fest kalkulierbaren Bedingungen.<br>
                        Ein Reparatur wir teuerer als erwartet? Auch in diesem  Fall ist eine Finanzierung möglich.
                    </p>
                </div>
                <div class="col-md-3">
                     <img src="www/gfx/sales/versicherung.jpg" class="img-responsive img-circle center-block salesicon">
                    <h4>Versicherung</h4>
                    <p class="text-muted">Sie haben sich entschlossen Ihren neues Auto zu finanzieren, möchten aber kein finanzielles Risiko eingehen? Unsere Verkaufberater können Ihnen ein passende KFZ-Versicherung mit anbieten.</p>
                </div>
                <div class="col-md-3">
                    <img src="www/gfx/sales/garantie.jpg" class="img-responsive img-circle center-block salesicon">
                    <h4>Garantie</h4>
                    <p class="text-muted">Unsere Verkaufsberater können Ihnen umfangreiche Angebote für Neuwagenschlussgarantienen und Gebrauchtwagengarantien unterbreiten</p>
                </div>
                 <div class="col-md-3">
                    <img src="www/gfx/sales/wartung.jpg" class="img-responsive img-circle center-block salesicon">
                    <h4>Wartung und Inspektion</h4>
                    <p class="text-muted">Top-Service zu einem günstigen Monatsbeitrag.<br>Der Service von kompetenten Fachleuten ist die beste Voraussetzung, dass Ihnen Ihr Fahrzeug möglichst lange erhalten bleibt.</p>
                </div>
            </div>
		</div>
    </div>
    </section>
    <section id="cars">
	<div class="container">
        <h2>Unsere Gebrauchtfahrzeuge:</h2>
		<div class="row">
            <div>
                Gebraucht fz .. qualität, blah schnell usw mobile bewertung....
            </div>
			<div class="well"><!-- noch anzupassen -->
			<div class="container-fluid">
  				<div id="fahrzeuge" class="carousel slide" data-ride="carousel" data-interval="2500">
					<div class="carousel-inner">
					<?php
						$count=1;
						foreach ( $this->data ['cars'] ['cars'] as $key => $car ) {
							foreach ( $car ['bilder'] as $bilder ) {
								if ($count == 1) {
									echo '<div class="item active">';
								} else {
									echo '<div class="item">';
								}
								echo '<div class="container-fluid">';
									echo '<div class="row">';
										echo '<div class="col-md-3"><img class="img-responsive" src="' . $bilder ['XL'] . '" alt="' . $car ['make_local_desc'] . ' ' . $car ['model_desc'] . '"></div>';
										echo '<div class="col-md-9">';
											echo '<h4>' . $car ['make_local_desc'] . ' ' . $car ['model_desc']  . '</h4>';
											echo '<p>';
												foreach($car['features'] as $feature) echo   $feature . ' / ';
											echo '</p>';
											echo '<h2>';
											echo $car ['preis']; 
											echo ' &euro;</h2>';
											if ($car ['vatable'] == 'true')
												echo '<span class="label label-warning">' . $car ['vatrate'] * 100 . '% MwSt.</span>';
											else
												echo '<span class="label label-warning">MwSt. nicht ausweisbar</span>';
										echo '</div>';
									echo '</div>';
								echo '</div>';
							echo '</div> <!-- Item -->';
						}
						$count ++;
					}
					?>	
       				</div><!-- Carousel inner -->
				</div><!-- Slide -->
			</div><!-- Container -->
			</div><!-- Well -->
		</div><!-- Row -->
		</div><!-- äußerer Container -->
</section>
<section id="parts">
    <div class="container">
        <h2>Original Teile und ZHubehör:</h2>
		<div class="row">
             Teile und Zeugs
        </div>
    </div>
</section>
<section id="contact">
    <div class="container">
        <h2>Original Teile und ZHubehör:</h2>
		<div class="row">
             Teile und Zeugs
        </div>
    </div>
</section>

    
    
<!-- Modal Boxen -->
<!-- Portfolio Inspektion -->
<div class="portfolio-modal modal fade" id="ServiceModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="modal-body">
				<h2>Wartung + Service + Inspektion</h2>
				<p>
					Regelmäßige Inspektionen erneuern die umfangreiche Mobilitätsgarantie des Herstellers und unterstützen Werterhalt und Zuverlässigkeit Ihres Fahrzeuges.<br>
					Wir kontrollieren und warten alle Komponenten und Baugruppen gründlich und zuverlässig nach Vorgabe des Herstellers.<br><br>
					Wann der nächste Service fällig ist zeigt Ihnen ein Aufkleber am Türholm bzw. entnehmen Sie Ihrem Serviceplan.<br>
					Alle an Ihrem Fahrzeug durchgeführten Servicearbeiten werden in diesem Plan dokumentiert.<br>
					Eine Serviceintervall-Anzeige im Tachometer erinnert Sie rechtzeitig vor einem fälligen Serviceereignis einen Werkstatttermin zu vereinbaren.<br><br>
					Nutzen Sie zum Werkstatttermin die Dialogannahme um gemeinsam mit Ihrem Serviceberater direkt am Fahrzeug den Arbeitsumfang für den anstehenden Service zu ermitteln.
				</p>
				<h2>Dialogannahme</h2>
				<p>
					Zusammen mit Ihnen gehen unsere Serviceberater Punkt für Punkt den Zustand Ihres Autos durch, nehmen Ihre Wünsche und Beanstandungen auf und legen erst dann den Reparaturumfang fest.<br>
					Sie werden sofort darüber Informiert was aus Sicherheitsgründen getan werden muss oder was eventuell noch bis zum nächsten mal warten kann.<br><br>
					Nutzen Sie die Dialogannahme bei Ihrem nächsten Service<br>
				</p>
			</div>
			<?php shophours('service', $this->data)?><br><br>	
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> schliesen</button>
		</div>
	</div>
</div>

<!-- Portfolio NotdienstModal -->
<div class="portfolio-modal modal fade" id="NotdienstModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="modal-body">
                <h2>24h Notdienst</h2>
				<p>
				    Eine Panne, einen Unfall. Der Volkswagen Notdienst hilft Ihnen 24 Stunden 7 Tage die Woche.<br><br>
				</p>
                <a class="btn btn-lg btn-warning" href="tel:0800897378423"><span class="glyphicon glyphicon-earphone"></span>0800 897378423 - Im Inland kostenlos</a>
                <br>
                <br>
                <div class="well shadow">
                    <h4>Pannen- und Unfallhilfe für Gehörlose</h4>
                    <h6>Per SMS</h6>
                    99-0800-1234107 (D1/D2)<br><br>
                    1551-0800-1234107 (E-Plus)<br><br>
                    329-0800-1-234107 (o2)<br><br>
                </div>
            </div>
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> schliesen</button>
		</div>
	</div>
</div>
  
<!-- Portfolio Fahrzeugvermietung -->
<div class="portfolio-modal modal fade" id="Ersatzwagenmodal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="modal-body">
                <h2>Ersatzwagen</h2>
				<p>
				    Einfach zum Servicetermin den Ersatzwagen mitbuchen – günstiger als Sie denken.
                    So bleiben Sie unabhängig und mobil. Keine Zeit, das Auto zu bringen? Verlangen Sie unseren.<br><br>
                    Wir machen alles möglich, damit Sie mobil bleiben.
                </p>
            </div>
            <?php shophours('service', $this->data)?><br><br>	
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> schliesen</button>
		</div>
	</div>
</div>
    
<!-- Portfolio Hol und Bringservice -->
<div class="portfolio-modal modal fade" id="HolundBringModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="modal-body">
                <h2>Hol-& Bringservice</h2>
				<p>
                    Auf eine Reparatur oder die Inspektion warten?<br><br>
                    Sie haben bestimmt etwas Besseres zu tun. Wir holen Ihr Auto bei Ihnen ab und bringen es nach dem Service wieder zurück.<br>
                    Oder Sie bringen Ihre Fahrzeug zu uns. Und wir bringen Sie nach Hause oder zur Arbeit.<br>
                    Ob bei Ihnen zu Hause oder bei der Arbeit, wir sind flexibel.<br>
                    Oder Sie bleiben auf Wunsch mit einem Ersatzwagen von uns mobil.<br>
                    Den Ersatzwagen bringen wir Ihnen gleich mit, wenn wir Ihr Fahrzeug abholen. Wenn wir Ihnen Ihr Auto zurückbringen, nehmen wir den Ersatzwagen wieder mit.<br>
                    Werkstattersatzwagen bekommen Sie bei uns zum günstigen Tarifen.<br><br>
                    Buchen Sie den Hol- und Bring Service gleich mit!<br>
                </p>
            </div>
            <?php shophours('service', $this->data)?><br><br>	
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> schliesen</button>
		</div>
	</div>
</div>
  
<!-- Portfolio Hauptuntersuchung -->
<div class="portfolio-modal modal fade" id="hauptuntersuchung" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="modal-body">
                <h2>Die Hauptuntersuchung</h2>
				<p>
                    Die Hauptuntersuchung ist täglich in unserem Autohaus möglich.<br> 
					Teilen Sie uns einfach telefonisch oder per E-Mail Ihren HU und/oder AU Termin mit und überlassen uns am vereinbarten 
					Tag Ihr Auto.<br>
					Wir wickeln die Abnahme der Hauptuntersuchung (nach §29 StVZO) und der Abgasuntersuchung (nach $47 StVZO)<br>
					in unserem Haus in Zusammenarbeit mit einer amtlich anerkannten Überwachsorganisation ab.<br>
					Falls Mängel festgestelltwerden versuchen wir gemeinsam mit Ihnen eine Lösung zu finden damit möglich Nachuntersuchungen<br>
					vermieden werden können und Ihr Fahrzeug schnellstmöglich wieder einsatzbereit ist.<br>
					Die Hauptuntersuchung wird durch Prüfingenieure der amtlich anerkannten Überwachungsorganisation <a href="http://www.tuv.com/de/deutschland/home.jsp">TÜV kPfalz Verkehrswesen GmbH</a>.<br><br>
					Damit Sie sorglos weiterfahren können erinnern wir Sie auch in Zukunft rechtzeitig über anstehende HU Untersuchungen.
                </p>
            </div>
            <?php shophours('service', $this->data)?><br><br>	
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> schliesen</button>
		</div>
	</div>
</div>

<!-- Portfolio Hauptuntersuchung -->
<div class="portfolio-modal modal fade" id="reifeneinlagernmodal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="close-modal" data-dismiss="modal">
				<div class="lr">
					<div class="rl"></div>
				</div>
			</div>
			<div class="modal-body">
                <h2>Reifen- und Räderwechsel - Einlagerungsservice</h2>
				<p>
				Zweimal im Jahr werden die Räder Ihres Fahrzeugs gewechselt.<br>
				Wir wechseln Räder und Reifen und Lagern sie auf Wunsch auch ein. So sparen Sie Platz in Ihrer Garage oder in ihrem Keller und müssen die Räder nicht transportieren.<br>
				Vor der Montage bzw. dem Einlagern prüfen wir die Reifen auf Beschädigungen, Alter und Verschleiß damit Sie beim nächsten Wechsel sicher weiterfahren können - egal ob Sommer oder Winter. </p>
				<ul>
					<li>fachgrechte Montage der Räder und Reifen</li>
  					<li>keine Räder, die im Keller oder in der Garage Platz verstellen</li>
    				<li>sicher eingelagerte Räder, die rechzeitig zum nächten Wechsel bereit stehen</li>
				</ul>
               
            </div>
            <?php shophours('service', $this->data)?><br><br>	
            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> schliesen</button>
		</div>
	</div>
</div>
    
   

	<!-- Footer -->
	<?php include 'static/foot_content.php'; ?>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
    <script>
    
    
(function($) {
    

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a').click(function(){ 
            $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    });
    
    $(window).scroll(function(){
         z =$(this).scrollTop();
         $('.c').css({ 
             'transform' : 'translate(' + z /100 + '%, 0px)'
         });
	   console.log('>' + z);
	
	})

})(jQuery);
    
    </script>
</body>
</html>
<?php
function shophours($shop, $data) {
	echo '<h4>Unser Service :</h4>';
	$key = key ( $data [$shop] ['zeiten'] );
	$zeiten = $data [$shop] ['zeiten'] [$key]->times;
	foreach ( $zeiten as $zeit )
		if ($zeit->lang == 'de')
			echo '<data  value="' . $zeit->datetime . '">' . $zeit->days . ':' . $zeit->time . '</data><br>';
	$key = key ( $data [$shop] ['kontakt'] );
	$contact = $data [$shop] ['kontakt'] [$key];
	echo '<br><a class="btn btn-primary" href="index.php?view=call&id=' . $contact->id . '"><span class="glyphicon glyphicon-envelope"></span> E-Mail</a> ';
	echo '<a class="btn btn-primary" href="tel:' . rawurlencode ( $contact->phone ) . ' "><span class="glyphicon glyphicon-earphone"></span>' . $contact->phone . '</a>';
}
?>	                			

