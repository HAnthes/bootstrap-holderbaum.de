<?php
/*
 * Redirector f�r Mailto Links
 * FF zeigt die Seite an, Chrome zb. nicht.
 * zur sicherheit ein "best�tung" und ein zur�ckbutton eingef�gt
 * error wenn Probleme auftreten, wird gnutzt um die Weiterleitung auszuf�hren oder zu verhindern.
 *
 */
?>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/default.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-06-21">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&#246;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">

<!-- Twitter -->
<meta name="twitter:title"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta name="twitter:url" content="www.holderbaum.de">

<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>Mail an Autohaus am Stadtpark Holderbaum GmbH - Volkswagen
	Service mitten in Kaiserslautern</title>
</head>

<body>
	<!-- Navigatipn -->     
	<?php include 'static/navbar_content.php'; ?>
	 <div class="container">
		<div class="row">
			<div class="well">
				<h1>Mail an :</h1>
				<div class="jumbotron shadow">
               	<?php
																foreach ( $this->data ['gf'] as $gf ) {
																	echo $gf->name . '<br>';
																	echo '<a href="tel:' . rawurlencode ( $gf->phone ) . ' "><span class="glyphicon glyphicon-earphone"></span> ' . $gf->phone . '</a><br>';
																	echo '<span class="glyphicon glyphicon-print"></span> ' . $gf->fax;
																}
																?>
        		<div class="text-center">
						<div class="btn btn-success" onclick="window,history.back();">
							<span class="glyphicon glyphicon-step-backward"></span>
							zur&uuml;ck
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php include 'static/foot_content.php'; ?>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>
<?php
if (! $this->data ['error']) {
	
	foreach ( $this->data ['gf'] as $gf ) {
		header ( 'Location: mailto:' . $gf->mail );
	}
}

?>
