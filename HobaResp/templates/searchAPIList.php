<?php
/*
 * Template f�r Listenansicht der Fahrzeuge.
 * Ajax f�r Filterknopf.
 * Todo : Header mit Navigation.
 * Todo : Metainformationen auslagern?
 *
 * Seite Validiert.
 */
?>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/default.css" rel="stylesheet">
<link href="www/cssAddon/searchAPIList.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-06-21">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&#246;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="Unser aktueller Fahrzeugbestand">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">

<!-- Twitter -->
<meta name="twitter:title" content="Gebrauchtfahrzeuge">
<meta name="twitter:url" content="www.holderbaum.de">

<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

<title>Die akutelle Fahrzeugliste vom Autohaus am Stadpark Holderbaum
	GmbH - Volkswagen Service mit in Kaiserslautern</title>
</head>

<body>
       <?php include 'static/navbar_content.php'; ?>
    
<!--   <section id="Fahrzeugliste"> -->
	<h2 class="hidden">Die akutelle Fahrzeugliste vom Autohaus am Stadpark
		Holderbaum GmbH - Volkswagen Service mitten in Kaiserslautern</h2>
	<div class="container">
		<div class="row">
            <?php
												// Sind Fahrzeuge in der Liste -> nein Jumbtron Fehler / Ja Liste
												if (count ( $this->data ['cars'] ['cars'] ) == 0) {
													?>
            <!-- Fehlermeldung: keine FZ gefiltert -->
			<div class="alert alert-danger text-center">
				Keine passenden Fahrzeuge gefunden!<br>
				<!--fab Button Filter -->
				<div id="fab" class="fab" data-toggle="modal" data-target="#filter">
					<span class="glyphicon glyphicon glyphicon-filter"></span>
				</div>
			</div>
            <?php
												
} else {
													?>
            <!-- Fahrzeugboxen -->

			<div class="panel-group">
                <?php
													foreach ( $this->data ['cars'] ['cars'] as $key => $car ) {
														?>
               <section id="<?php echo $key; ?>">
					<div class="panel panel-default shadow space" itemscope
						itemtype="http://schema.org/Car">
						<!-- Box Überschrift -->
						<div class="panel-heading">
							<div class="panel-title pull-left">
								<a href="index.php?view=ad&ad=<?php echo $key; ?>"
									itemprop="url">
									<h4 itemprop="name"><?php echo $car['make_local_desc'] . ' ' . $car['model_desc']; ?></h4>
								</a>
							</div>
							<div class="panel-title pull-right ">
								<a href="index.php?view=ad&ad=<?php echo $key; ?>"
									itemprop="url"> <span
									class="glyphicon glyphicon glyphicon-zoom-in"></span>
								</a>
							</div>
							<div class="clearfix"></div>
						</div>
						<!-- Box Boddy -->
						<div class="panel-body">
							<div class="row">
								<!-- Bild -->
								<div class="col-sm-4">
									<div class="thumbnail">
                                <?php
														foreach ( $car ['bilder'] as $bilder ) {
															echo '<a href="index.php?view=ad&ad=' . $key . '">';
															echo '<img class="img-responsive " itemprop="image" src="' . $bilder ['L'] . '" alt="' . $car ['make_local_desc'] . ' ' . $car ['model_desc'] . '">';
															echo '</a>';
														}
														?>
                                </div>
								</div>
								<!-- Erste Spalte mit Daten -->
								<div class="col-sm-8">
									<div>
										<time itemprop="dateVehicleFirstRegistered"
											datetime="<?php $temp = new DateTime($car['first-registration']); echo $temp->format('Y-m-d')?>">
                                        EZ : <?php $temp = new DateTime($car['first-registration']); echo $temp->format('m/Y')?>
                                    </time>, 
                                        HU : <?php $temp = new DateTime($car['general-inspection']); echo $temp->format('m/Y')?>
                                    <span itemprop="mileageFromOdometer"><?php echo $car['mileage']?> km</span>,
								    <?php echo $car['power']?> kW,
                                    <?php echo $car['gearbox_local_desc']?>,
                                    <span itemprop="fuelType"><?php echo $car['fuel_local_desc']?></span>,
                                    <?php echo $car['climatisation_local_desc']?>
								</div>
									<hr>
									<div>
                                    <?php foreach($car['features'] as $feature) echo   $feature . ' / ';?>
                                </div>
									<div class="text-right">
										<div itemprop="offers" itemscope
											itemtype="http://schema.org/Offer">
                                        <?php echo '<span itemprop="price" class="prize">' . $car ['preis'] . '</span>';?>
                                            <span
												itemprop="priceCurrency" content="EUR"><strong>&euro;</strong></span>
										</div>
                                    <?php
														if ($car ['vatable'] == 'true')
															echo '<span class="label label-warning">' . $car ['vatrate'] * 100 . '% MwSt.</span>';
														else
															echo '<span class="label label-warning">MwSt. nicht ausweisbar</span>';
														?>
                                </div>
								</div>


							</div>
						</div>
					</div>
				</section>
				<?php  } // For Beenden ?>
                </div>        
                <?php     } //IF Benden?>
 		
            
            <!--fab Button Filter -->
			<div id="fab" class="fix fab up" data-toggle="modal"
				data-target="#filter">
				<span class="glyphicon glyphicon glyphicon-filter"></span>
			</div>


			<!-- Seitenschaltung -->
			<section id="pageingation">
				<h6 class="hidden">Seitenweiterschaltung</h6>
				<div class="text-center">
            	<?php echo pager($this->data);?>
 			</div>
			</section>
		</div>
	</div>
	<!--  </section> -->

	<!-- Footer -->
 <?php include 'static/foot_content.php'; ?>

 
<!-- Modal Dialog Filter-->
	<section id="Filterdialog">
		<div class="modal fade" id="filter" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<form action="index.php?view=list" method="post" id="filters">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Filter:</h4>
						</div>
						<div class="modal-body">
          
                 <?php 
/* Marken Auswahl */
																	foreach ( $this->data ['make'] as $element ) {
																		?>
                         <div class="inline">
								<label class="checkbox-inline"> <input type="checkbox"
									name="make[]" value="<?php echo $element['name'] ?>"
									<?php echo $element['value']?>> <span class="cb"> <img
										src="www/gfx/SearchAPI/make/<?php echo strtolower($element['name']) .'_logo.jpg' ?>"
										alt="<?php echo $element['name'] ?>">
								</span>
								</label>
							</div>
					<?php }?>
                   <hr>
                   <?php 
/* Aufbau Auswahl - Wegen den Bilder PHP aufgetrennt */
																			foreach ( $this->data ['cat'] as $element ) {
																				?>              
				   <div class="inline">
								<label class="checkbox-inline"> <input type="checkbox"
									name="cat[]" value="<?php echo $element['name'] ?>"
									<?php echo $element['value'] ?>> <span class="cb"> <img
										src="www/gfx/SearchAPI/cat/<?php echo $element['name'] .'.png' ?>"
										alt="<?php echo $element['name'] ?>"> <span
										class="text-center block"><?php echo $element['key']?></span>
								</span>
								</label>
							</div>		    
					<?php }?>
                   <hr>
                   <?php 
/* Treibstoff */
																			foreach ( $this->data ['fuel'] as $element ) {
																				echo ' <div class="inline"><label class="checkbox-inline"><input type="checkbox" name="fuel[]" value="' . $element ['name'] . '" ' . $element ['value'] . '><span class="cb">' . $element ['desc'] . '</span></label></div>';
																			}
																			
																			?>
                   <hr>
                   <?php 
/* Klima */
																			foreach ( $this->data ['clima'] as $element ) {
																				echo ' <div class="inline"><label class="checkbox-inline"><input type="checkbox" name="clima[]" value="' . $element ['name'] . '" ' . $element ['value'] . '><span class="cb">' . $element ['desc'] . '</span></label></div>';
																			}
																			
																			?>
                </div>

						<div class="modal-footer">
							<div class="panel-title pull-left">
								<button type="submit" class="btn btn-success">
									<span id="counter"><?php echo $this->data['cars']['total'] ?></span>
									Treffer <span class="glyphicon glyphicon-search"></span>
								</button>
							</div>
							<div class="panel-title pull-right">
								<button type="button" class="btn btn-danger"
									data-dismiss="modal">schliessen</button>
							</div>
							<div class="clear-fix"></div>
						</div>
					</form>

				</div>
			</div>
		</div>
	</section>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="www/js/searchAPIList.js"></script>
</body>
</html>

<?php
function pager($data) {
	$temp = '<nav><ul class="pagination pagination-lg">';
	$s = '1';
	if (intval ( $data ['cars'] ['seite'] ) > 1)
		$s = intval ( $data ['cars'] ['seite'] ) - 1;
	$temp .= '<li><a href="index.php?view=list&page=' . $s . $data ['filters'] . '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>';
	for($pages = 1; $pages <= $data ['cars'] ['max']; $pages ++) {
		if ($data ['cars'] ['seite'] == $pages) {
			$temp .= '<li class="active">';
		} else {
			$temp .= '<li>';
		}
		$temp .= '<a href="index.php?view=list&page=' . $pages . $data ['filters'] . '">' . $pages . '</a></li>';
	}
	$s = $data ['cars'] ['max'];
	
	if ($data ['cars'] ['seite'] < $s)
		$s = intval ( $data ['cars'] ['seite'] ) + 1;
	$temp .= '<li><a href="index.php?view=list&page=' . $s . $data ['filters'] . '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li></ul></nav>';
	return $temp;
}
?>