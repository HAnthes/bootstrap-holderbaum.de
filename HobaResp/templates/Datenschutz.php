<?php
/*
 * Templagte f�r das Impressum / Datenschutz.
 * gf = Kontakte
 *
 */
?>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/default.css" rel="stylesheet">
<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-06-21">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&#246;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="Datenschutz Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">

<!-- Twitter -->
<meta name="twitter:title"
	content="Datenschutz Autohaus am Stadtpark Holderbaum GmbH">
<meta name="twitter:url" content="www.holderbaum.de">

<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>Datenschutz - Autohaus am Stadtpark Holderbaum GmbH - Volkswagen
	Service mitten in Kaiserslautern</title>
</head>

<body>
	<!-- Navigatipn -->     
	<?php include 'static/navbar_content.php'; ?>
	 <div class="container">
		<div class="row">
			<div class="well">
				<h1>Datenschutz</h1>
				<div class="panel-group">
					<div class="panel panel-default shadow space">
						<div class="panel-body">
							<p>Wir nehmen den Schutz Ihrer pers&ouml;nlichen Daten sehr ernst
								und halten uns strikt an die Regeln der Datenschutzgesetze. Die
								nachfolgende Erkl&auml;rung gibt Ihnen einen &Uuml;berblick
								dar&uuml;ber, wie wir diesen Schutz gew&auml;hrleisten und
								welche Art von Daten zu welchen Zwecken erhoben werden.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Anonyme Datenerhebung</h3>
						</div>
						<div class="panel-body">
							<p>In der Regel k&ouml;nnen Sie unsere besuchen, ohne dass wir
								pers&ouml;nliche Daten von Ihnen ben&ouml;tigen. Erhoben und
								speichert werden Informatonen die Ihr Browser an uns
								&uuml;bermittelt. Dies sind:</p>
							<ul>
								<li>Browsertyp/ -version</li>
								<li>Verwendetes Betriebssystem</li>
								<li>Referer URL</li>
								<li>Datum und Uhrzeit der Serveranfrage</li>
								<li>Abgerufene Seite</li>
							</ul>
							<p>Diese Informationen werden zu statistischen Zwecken
								ausgewertet. Sie bleiben als Nutzer hierbei anonym.Dar&uuml;ber
								hinausgehende personenbezogene Angaben wie Ihr Name, Ihre
								Anschrift, Telefonnummer oder E-Mail-Adresse werden nicht
								erfasst, es sei denn, Sie machen diese Angaben freiwillig, z.B.
								im Rahmen einer Informationsanfrage.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Automatisch erfasste, nicht personenbezogene Daten; aktive
								Inhalte</h3>
						</div>
						<div class="panel-body">
							<p>Beim Zugriff auf unsere Webseiten, werden automatisch erzeugte
								Informationen gesammelt, die nicht einer bestimmten Person
								zugeordnet werden k&ouml;nnen (z. B. &uuml;ber einen
								Internetbrowser oder ein Betriebssystem; Domainnamen von
								Webseiten, von denen aus &uuml;ber einen Weblink ein Zugriff
								erfolgt; Anzahl der Besucher durchschnittliche Verweilzeit;
								aufgerufene Seiten). Wir verwenden diese Informationen, um den
								Auftritt unserer Webseite st&auml;ndig zu verbessern, zu
								aktualisieren und somit ihre Attraktivit&auml;t zu erh&ouml;hen.
								Wenn Sie &uuml;ber einen Link unsere Seite verlassen und so auf
								fremde Seiten gelangen, kann es sein, dass auch von Adressaten
								der angeklickten Zielseite Cookies gesetzt werden. F&uuml;r
								diese Cookies sind wir rechtlich nicht verantwortlich. Bei der
								Bereitstellung dieses Internetangebots wird Java-Script
								verwendet. Falls Sie sich aus Sicherheitsgr&uuml;nden dieses
								nicht nutzbar machen wollen, sollten Sie die entsprechende
								Einstellung Ihres Browsers deaktivieren.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Nutzung und Weitergabe personenbezogener Daten</h3>
						</div>
						<div class="panel-body">
							<p>Soweit Sie uns personenbezogene Daten zur Verf&uuml;gung
								gestellt haben, verwenden wir diese ausschlie&szlig;lich zum
								Zweck der technischen Administration unserer Webseiten und zur
								Erf&uuml;llung Ihrer W&uuml;nsche und Anforderungen, also in der
								Regel die Beantwortung Ihrer Anfragen.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Formulare</h3>
						</div>
						<div class="panel-body">
							<p>Die Inhalte der Formulare werden lokal in einer Datenbank
								gespeichert, um Ihre Anfragen bearbeiten zu k&ouml;nnen.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Cookies und Website-Tracking</h3>
						</div>
						<div class="panel-body">
							<p>Dar&uuml;ber hinaus werden auch Cookies vergeben. Ein Cookie
								ist eine kleine Datei, die auf Ihrem Computer gespeichert wird,
								sobald Sie eine Website besuchen. Wenn Sie die Website erneut
								besuchen, zeigt der Cookie an, dass es sich um einen
								wiederholten Besuch handelt. Der Cookie enth&auml;lt keine
								pers&ouml;nlichen Angaben. Der Cookie ist nicht geeignet, Sie
								auf den Websites Dritter zu identifizieren. Weiterhin
								k&ouml;nnen Cookies dazu verwendet werden, von Ihnen bevorzugte
								Einstellungen, wie z.B. Sprach- und Landeseinstellungen zu
								speichern, damit diese bei Ihrem n&auml;chsten Besuch gleich zur
								Verf&uuml;gung stehen. Wir verwenden Cookies nicht, um Sie
								pers&ouml;nlich zu identifizieren!</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Sicherheit</h3>
						</div>
						<div class="panel-body">
							<p>Wir setzten technische und organisatorische
								Sicherungsma&szlig;nahmen ein, um Ihre uns zur Verf&uuml;gung
								gestellten Daten, durch zuf&auml;llige oder vors&auml;tzliche
								Manipulation, Verlust, Zerst&ouml;rung oder den Zugriff
								unberechtigter Personen zu sch&uuml;tzen. Unsere
								Sicherheitsma&szlig;nahmen werden entsprechend der
								technologischen Entwicklung fortlaufend verbessert.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Externe Links</h3>
						</div>
						<div class="panel-body">
							<p>Zu Ihrer optimalen Information finden Sie auf unseren Seiten
								Links, die auf Seiten Dritter verweisen. Soweit dies nicht
								offensichtlich erkennbar ist, weisen wir darauf hin, dass es
								sich um einen externen Link handelt. Wir haben keinen Einfluss
								auf den Inhalt und die Gestaltung dieser Seiten anderer
								Anbieter. Die Garantien dieser Datenschutzerkl&auml;rung gelten
								daher selbstverst&auml;ndlich dort nicht.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Google Analytics</h3>
						</div>
						<div class="panel-body">
							<p>Diese Website benutzt Google Analytics, einen Webanalysedienst
								der Google Inc. (&#8222;Google&ldquo;). Google Analytics
								verwendet sog. &#8222;Cookies&ldquo;, Textdateien, die auf Ihrem
								Computer gespeichert werden und die eine Analyse der Benutzung
								der Website durch Sie erm&ouml;glichen. Die durch den Cookie
								erzeugten Informationen &uuml;ber Ihre Benutzung dieser Website
								werden in der Regel an einen Server von Google in den USA
								&uuml;bertragen und dort gespeichert.</p>

							<p>Im Falle der Aktivierung der IP-Anonymisierung auf dieser
								Webseite, wird Ihre IP-Adresse von Google jedoch innerhalb von
								Mitgliedstaaten der Europ&auml;ischen Union oder in anderen
								Vertragsstaaten des Abkommens &uuml;ber den Europ&auml;ischen
								Wirtschaftsraum zuvor gek&uuml;rzt. Nur in Ausnahmef&auml;llen
								wird die volle IP-Adresse an einen Server von Google in den USA
								&uuml;bertragen und dort gek&uuml;rzt. Die IP-Anonymisierung ist
								auf dieser Website aktiv. Im Auftrag des Betreibers dieser
								Website wird Google diese Informationen benutzen, um Ihre
								Nutzung der Website auszuwerten, um Reports &uuml;ber die
								Websiteaktivit&auml;ten zusammenzustellen und um weitere mit der
								Websitenutzung und der Internetnutzung verbundene
								Dienstleistungen gegen&uuml;ber dem Websitebetreiber zu
								erbringen.</p>

							<p>
								Die im Rahmen von Google Analytics von Ihrem Browser
								&uuml;bermittelte IP-Adresse wird nicht mit anderen Daten von
								Google zusammengef&uuml;hrt. Sie k&ouml;nnen die Speicherung der
								Cookies durch eine entsprechende Einstellung Ihrer
								Browser-Software verhindern; wir weisen Sie jedoch darauf hin,
								dass Sie in diesem Fall gegebenenfalls nicht s&auml;mtliche
								Funktionen dieser Website vollumf&auml;nglich werden nutzen
								k&ouml;nnen. Sie k&ouml;nnen dar&uuml;ber hinaus die Erfassung
								der durch das Cookie erzeugten und auf Ihre Nutzung der Website
								bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die
								Verarbeitung dieser Daten durch Google verhindern, indem sie das
								unter dem folgenden Link verf&uuml;gbare Browser-Plugin
								herunterladen und installieren: <a
									href="http://tools.google.com/dlpage/gaoptout?hl=de">http://tools.google.com/dlpage/gaoptout?hl=de</a>
							</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Auskunftsrecht</h3>
						</div>
						<div class="panel-body">
							<p>Sie haben jederzeit das Recht auf Auskunft &uuml;ber die
								bez&uuml;glich Ihrer Person gespeicherten Daten, deren Herkunft
								und Empf&auml;nger sowie den Zweck der Datenverarbeitung.
								Auskunft &uuml;ber die gespeicherten Daten gibt der
								Datenschutzbeauftragte in unserem Haus.</p>
						</div>
					</div>
					<div class="panel panel-default shadow space">
						<div class="panel-heading">
							<h3>Datenschutzbeauftragter</h3>
						</div>
						<div class="panel-body">
							<p>Ihr Vertrauen ist uns wichtig. Daher m&ouml;chten wir Ihnen
								jederzeit Rede und Antwort stehen. Wenn Sie Fragen hinsichtlich
								der Verarbeitung Ihrer pers&ouml;nlichen Daten haben,
								k&ouml;nnen Sie sich direkt an unseren Beauftragten f&uuml;r den
								Datenschutz wenden, der auch im Falle von Auskunftsersuchen,
								Antr&auml;gen oder Beschwerden zum Thema Datenschutz zur
								Verf&uuml;gung steht:</p>
							<p>datenschutz@holderbaum.de</p>
						</div>
					</div>




				</div>
				<!-- Panel-Group -->
			</div>
		</div>
	</div>

	<!-- Footer -->
	<?php include 'static/foot_content.php'; ?>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>