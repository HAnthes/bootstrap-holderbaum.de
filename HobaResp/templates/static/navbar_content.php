
<nav id="mainNav" class="navbar navbar-default navbar-fixed-top shadow">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand page-scroll" href="index.html"><img
				class="img-responsive" src="www/gfx/default/logo32.png"
				alt="Logo Holderbaum"></a>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li><a class="page-scroll" href="#gutachten">Service</a></li>
				<li><a class="page-scroll" href="index.php?view=list">Verkauf</a></li>
				<li><a class="page-scroll" href="#services">Zubeh&ouml;r und
						Ersatzteile</a></li>
			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container-fluid -->
</nav>
