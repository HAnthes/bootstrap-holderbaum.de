<!-- Footer Schwarz Externe Links -->
<footer id="footer">
	<div class="container">
		<div class="row black">
			<div class="btn-group btn-group-justified">
				<a href="index.php?view=Kontakt" class="btn btn-black">Kontakt</a>
				<a href="index.php?view=USKunden" class="btn btn-black">US-Customers</a>
				<a href="index.php?view=Impressum" class="btn btn-black">Impressum</a>
				<a href="index.php?view=Datenschutz" class="btn btn-black">Datenschutz</a>
				
				
			</div>
		</div>
		<div class="row black">
			<div class="btn-group btn-group-justified">
				<a href="https://www.facebook.com/Autohaus.Holderbaum/"
					class="btn btn-black"><i class="fa fa-fw fa-facebook"></i> facebook</a>
				<a
					href="https://www.google.de/maps/place/Autohaus+am+Stadtpark+Holderbaum+GmbH/@49.439068,7.764018,15z/data=!4m5!3m4!1s0x0:0x4d7ae84f76da6530!8m2!3d49.439068!4d7.764018"
					class="btn btn-black"><i class="fa fa-fw fa-google"></i>google</a>
				<a href="https://www.mobile.de/bewertungen/HOLDERBAUM#1" class="btn btn-black">mobile.de</a>
				<a
					href="http://haendler.autoscout24.de/autohaus-am-stadtpark-holderbaum-gmbh"
					class="btn btn-black">Autoscout24</a>
			</div>
		</div>
	</div>
</footer>