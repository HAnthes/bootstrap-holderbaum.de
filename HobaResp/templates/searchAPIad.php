<?php
/*
 * Template f�r Einzelfahrzeug
 */
?>
<!DOCTYPE html>
<html lang="de">

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="www/cssAddon/default.css" rel="stylesheet">
<link href="www/cssAddon/searchAPIad.css" rel="stylesheet">

<link
	href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css"
	rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Nunito:400,300,700'
	rel='stylesheet' type='text/css'>

<!-- Meta allg. -->
<meta name="robots" content="index,follow">
<meta name="author" content="Hans Anthes">
<meta name="publisher" content="AH Holderbaum GmbH">
<meta name="date" content="2016-06-21">
<meta name="copyright" content="Hans Anthes">
<meta name="generator" content="BSSA">
<meta name="audience" content="Alle">
<meta name="abstract"
	content="Gebrauchtwagem Jahreswagen, Volkswagen Service, Partner, Orginal Teile Zubeh&#246;r">
<meta name="page-type" content="Produktinfo">
<meta name="page-topic" content="Automobilhandel und Werkstatt">
<meta name="revisit-after" content="1 days">
<meta name="audience" content="Alle">
<meta name="geo.region" content="DE-RP">
<meta name="geo.placename"
	content="Pirmasenser Strasse 57, 67655 Kaiserslautern, Deutschland">
<meta name="geo.position" content="49.43912;7.76419">
<meta name="ICBM" content="49.43912,7.76419">

<!-- Facebook -->
<meta property="og:description"
	content="<?php echo $this->data['cars']['make_local_desc'] .' '.  $this->data['cars']['model_desc']?>">
<meta property="og:image"
	content="<?php echo $this->data['cars']['bilder'][0]['XL'] ?>">
<meta property="og:url" content="www.holderbaum.de">
<meta property="og:site_name"
	content="Autohaus am Stadtpark Holderbaum GmbH">
<meta property="og:title"
	content="Autohaus am Stadtpark Holderbaum GmbH  - Ihr Volkswagen Service Partner in Kaiserslautern">
<meta property="og:type" content="website">
<meta property="og:locale" content="de_DE">
<!-- Twitter -->
<meta name="twitter:title"
	content="<?php echo $this->data['cars']['make_local_desc'] .' '.  $this->data['cars']['model_desc']?>">
<meta name="twitter:image"
	content="<?php echo $this->data['cars']['bilder'][0]['XL'] ?>">
<meta name="twitter:url" content="www.holderbaum.de">
<meta name="twitter:card" content="summary">

<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

<title>Ein Fahrzeug vom Autohaus am Stadpark Holderbaum GmbH -
	Volkswagen Service mitten in Kaiserslautern</title>
</head>

<body>
	<!-- Header Include -->
	<?php include 'static/navbar_content.php';?>
	<section id="Fahrzeug" itemscope itemtype="http://schema.org/Car">
		<!-- Ueberschrift H1 nicht anzeigen -->
		<h1 class="hidden">
			<span itemprop="brand"><?php echo $this->data['cars']['make_local_desc'] ?></span>
			<span itemprop="name"><?php echo $this->data['cars']['model_desc']?></span>
		</h1>
		<!-- H1 soll nicht angezeigt werden -->

		<div class="container">
			<!-- Toolbar / Zurueck usw -->
			<div class="row" id="toolbar">
				<div class="btn-group btn-group-justified shadow">
					<a href="index.php?view=list" class="btn btn-success"><i
						class="glyphicon glyphicon-backward"></i></a>
					<!-- Modal  social-->
					<a href="#" class="btn btn-default" data-toggle="modal"
						data-target="#social"><i class="fa fa-fw fa-facebook"></i></a> <a
						href="#" class="btn btn-default" data-toggle="modal"
						data-target="#social"><i class="fa fa-fw fa-google"></i></a> <a
						href="#" class="btn btn-default" data-toggle="modal"
						data-target="#social"><i class="fa fa-fw fa-pinterest"></i></a> <a
						href="#" class="btn btn-default" data-toggle="modal"
						data-target="#social"><i class="fa fa-fw fa-twitter"></i></a> <a
						href="#" class="btn btn-default" data-toggle="modal"
						data-target="#social"><i class="fa fa-fw fa-share-alt"></i></a>
					<!--  /Modal -->
					<a href="tel:+496313162556" class="btn btn-danger"><i
						class="glyphicon glyphicon-earphone"></i></a>
				</div>
			</div>

			<!-- Fahrzeug -->
			<div class="row">
				<div class="panel-group">
					<div class="panel panel-default shadow space">
						<!-- Bilder Kontakt und Preis -->
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $this->data['cars']['make_local_desc'] .' '.  $this->data['cars']['model_desc']?></h3>
						</div>
						<div class="panel-body">
							<div class="col-sm-8">
								<!-- Bilder -->
								<div id="<?php  echo $this->data['cars']['fin'] ?>"
									class="carousel slide" data-ride="carousel"
									data-interval="1500" data-pause="hover">
									<div class="x">
										<ol class="carousel-indicators">
											<li data-target="#<?php  echo $this->data['cars']['fin'] ?>"
												data-slide-to="0" class="active">1</li>
		                            <?php for($x=1; $x<count($this->data['cars']['bilder']);$x++){?>
		                                <li
												data-target="#<?php  echo $this->data['cars']['fin'] ?>"
												data-slide-to="<?php echo $x;?>">
		                                    <?php echo $x+1;?>
		                                </li>
		                                <?php } ?>
		                        </ol>
									</div>
									<div class="carousel-inner" role="listbox">
	                            <?php
																													$a = 1;
																													foreach ( $this->data ['cars'] ['bilder'] as $bilder ) {
																														?>
	                                <div
											class="item <?php if($a==1) echo 'active'?>">
											<img itemprop="image" src="<?php echo $bilder['XL'] ?>"
												alt="<?php echo $this->data['cars']['model_desc'];?>"
												class="img-responsive center-block">
										</div>
	                            <?php $a++;} ?>
	                        </div>
								</div>
							</div>
							<!-- Bilder Spalte sm 8 -->
							<div class="col-sm-4">
								<!-- Kontakt Informationen -->
								<div class="well">
									<div>
										<form id="response">
											<div class="form-group">
												<label for="email">Email:</label> <input type="email"
													name="email" class="form-control" id="email"
													placeholder="EMail Adresse" data-rule-required="true"
													data-rule-email="true"
													data-msg-required="Bitte geben Ihre EMailadresse an"
													data-msg-email="Bitte geben Ihre EMailadresse an"> <input
													type="email" name="vemail" class="hidden" id="vemail"
													placeholder="Verify Email">
											</div>
											<div class="form-group">
												<label for="email">Telefon:</label> <input type="tel"
													name="phone" class="form-control" id="telefon"
													placeholder="Ihre Telefonnummer">
											</div>
											<div class="form-group">
												<label for="messagebox">Nachricht:</label>
												<textarea class="form-control" name="messagebox"
													id="messagebox" required
													data-msg-required="Bitte geben Sie noch eine Nachricht ein."
													placeholder="Ihre Anfrage......"></textarea>
											</div>
											<button value="Submit" name="submit" type="submit"
												id="submit" class="btn btn-primary btn-lg">
												<span class="hidden"><img src="www/gfx/default/spinner.gif"
													alt="progressbar"></span> Senden
											</button>

											<input type="hidden" name="view" id="view" value="jform"> <input
												type="hidden" name="target" id="target" value="v">
										</form>

									</div>
								</div>
								<!-- Preis Informationen -->
								<div class="well" itemprop="offers" itemscope
									itemtype="http://schema.org/Offer">
									<div class="text-right">
										<h2>
		                                 
		                                 <?php echo '<span itemprop="price"><strong>' .$this->data['cars']['preis'] . '</strong></span>';?>
		                                 <span itemprop="priceCurrency"
												content="EUR"><strong>&euro;</strong></span>
										</h2>
		                             <?php
																															
if ($this->data ['cars'] ['vatable'] == 'true')
																																echo '<span class="label label-warning">' . $this->data ['cars'] ['vatrate'] * 100 . '% MwSt.</span>';
																															else
																																echo '<span class="label label-warning">Differenzbesteuert nach Par. 25a USTG</span>';
																															?>
		                        </div>
								</div>
								<!-- Preis Ende -->
							</div>
						</div>
						<!-- Kontakt und Preis Spalte Sm4 -->
					</div>


					<div class="panel panel-default shadow space">
						<!-- Umwelt/verbrauch -->
						<div class="panel-heading">
							<h4 class="panel-title">Umwelt/Verbrauch</h4>
						</div>
						<div class="panel-body" id="umwelt">
							<div class="table-responsive">
								<div class="col-sm-6">
									<table class="table table-hover">
										<tbody>
											<tr>
												<td><span>Treibstoff</span></td>
												<td></td>
												<td><span itemprop="fuelType"><?php echo $this->data['cars']['fuel_local_desc'] ?></span></td>
											</tr>
											<tr>
												<td><span>Schadstoffklasse</span></td>
												<td><span class="label label-info"> <span
														class="glyphicon glyphicon-eye-open" data-toggle="modal"
														data-target="#Schadstoffklasse"></span>
												</span></td>
												<td><span itemprop="meetsEmissionStandard"><?php echo $this->data['cars']['emission-class_local_desc'] ?></span></td>
											</tr>
											<tr>
												<td><span>Umweltplakette</span></td>
												<td></td>
												<td><span><?php echo $this->data['cars']['emission-sticker_local_desc'] ?></span></td>
											</tr>
                                     <?php
																																					// Nur Autoplenumg Link nur bei VW und Audi
																																					if ($this->data ['cars'] ['make_key'] == 'AUDI' || $this->data ['cars'] ['make_key'] == 'VW') {
																																						// Die Plus und Sportsvan Golf Filtern - f�r die Autoplenum url nur Golf!
																																						if (strpos ( $this->data ['cars'] ['model_key'], 'Golf' ) !== false) {
																																							$model = 'Golf';
																																						} else {
																																							$model = $this->data ['cars'] ['model_key'];
																																						}
																																						?> 
                                     <tr>
												<td colspan="3"><span itemprop="review" itemscope
													itemtype="http://schema.org/Review"> <a itemprop="Url"
														class="btn btn-primary"
														href="http://www.autoplenum.de/auto/<?php echo $this->data['cars']['make_key'] ?>/<?php echo $model?>">
															<span class="glyphicon glyphicon-link"></span> <span
															itemprop="name">Test <?php echo $this->data['cars']['make_key'] ?> <?php echo $model?> </span>
															auf <span itemprop="author">www.autoplenum.de</span>
													</a>
												</span></td>

											</tr>
                                     <?php }?>
                                    
                                </tbody>
									</table>
								</div>
								<!-- Tabelle 1 Umwelt -->
								<div class="col-sm-6">
									<table class="table table-hover">
										<tbody>
                          			<?php if ($this->data['cars']['emission-envkv_combined']!='' && $this->data['cars']['emission-envkv_combined']!='' && $this->data['cars']['emission-envkv_outer'] != '' && $this->data['cars']['emission-envkv_co2'] !='' ) { ?>
                                    	<tr>
												<td><span>Verbrauch</span></td>
												<td><span class="label label-info"> <span
														class="glyphicon glyphicon-eye-open" data-toggle="modal"
														data-target="#Verbrauch"></span>
												</span></td>
												<td></td>
											</tr>
											<tr>
												<td><span>kombiniert</span></td>
												<td></td>
												<td><span itemprop="fuelConsumption"><?php echo $this->data['cars']['emission-envkv_combined'] ?> l/100km</span></td>
											</tr>
											<tr>
												<td><span>innerorts:</span></td>
												<td></td>
												<td><span><?php echo $this->data['cars']['emission-envkv_inner'] ?> l/100km</span></td>
											</tr>
											<tr>
												<td><span>außerorts:</span></td>
												<td></td>
												<td><span><?php echo $this->data['cars']['emission-envkv_outer'] ?> l/100km</span></td>
											</tr>
											<tr>
												<td><span>CO2 Emission (mix):</span></td>
												<td></td>
												<td><span><?php echo $this->data['cars']['emission-envkv_co2'] ?>mg</span></td>
											</tr>
                                         <?php } ?>    
                                    </tbody>
									</table>
								</div>
								<!-- Spalten Div -->
							</div>
							<!-- Tabellen Div -->
						</div>
						<!-- Panel Body -->
					</div>
					<!-- Umwelt/Verbrauch -->
					<div class="panel panel-default shadow space">
						<!-- Technische Daten -->
						<div class="panel-heading">
							<h4 class="panel-title">Technische Daten</h4>
						</div>
						<div class="panel-body" id="Technischedaten">
							<div class="table-responsive">
								<div class="col-sm-6">
									<table class="table table-hover">
										<tbody>
											<tr>
												<td>Hubraum</td>
												<td><?php echo $this->data['cars']['cubic-capacity']?> ccm</td>
											</tr>
											<tr>
												<td>Leistung</td>
												<td><?php echo $this->data['cars']['power']?> kW</td>
											</tr>
											<tr>
												<td>Getriebe</td>
												<td><?php echo $this->data['cars']['gearbox_local_desc']?></td>
											</tr>
											<tr>
												<td>Laufleistung</td>
												<td><?php echo $this->data['cars']['mileage']?> km</td>
											</tr>
											<tr>
												<td>Erstzulassung</td>
												<td><time itemprop="dateVehicleFirstRegistered"
														datetime="<?php $temp = new DateTime($this->data['cars']['first-registration']); echo $temp->format('Y-m-d')?>">
                                                    <?php $temp = new DateTime($this->data['cars']['first-registration']); echo $temp->format('m/Y')?>
                                            </time></td>
											</tr>
											<tr>
												<td>Vorbesitzer</td>
												<td><span itemprop="numberOfPreviousOwners"><?php echo $this->data['cars']['previous-owners']?></span></td>
											</tr>
											<tr>
												<td>HU/AU</td>
												<td><?php $temp = new DateTime($this->data['cars']['general-inspection']); echo $temp->format('m/Y')?></td>
											</tr>
											<tr>
												<td>Anzahl der Türen</td>
												<td><span itemprop="numberOfDoors"><?php echo $this->data['cars']['door_desc']?></span></td>
											</tr>
										</tbody>
									</table>
								</div>
								<div class="col-sm-6">
									<table class="table table-hover">
										<tbody>
											<tr>
												<td>Airbags</td>
												<td><?php echo $this->data['cars']['airbag_local_desc']?></td>
											</tr>
											<tr>
												<td>Einparkhilfen</td>
												<td><?php
												
foreach ( $this->data ['cars'] ['parkingassistants'] as $system ) {
													echo $system . ', ';
												}
												?>
                                		</td>
											</tr>
											<tr>
												<td>Klimatisierung</td>
												<td><?php echo $this->data['cars']['climatisation_local_desc']?></td>
											</tr>
											<tr>
												<td>Anzahl der Sitze</td>
												<td><?php echo $this->data['cars']['num-seats']?></td>
											</tr>
											<tr>
												<td>Farbe</td>
												<td><span itemprop="color"><?php echo $this->data['cars']['exterior-color_name']?></span></td>
											</tr>
											<tr>
												<td>Innenausstattung</td>
												<td><?php echo $this->data['cars']['interior-color_local_desc']?>, <?php echo $this->data['cars']['interior-type_local_desc']?></td>
											</tr>
											<tr>
												<td>KBA-NR</td>
												<td><?php echo $this->data['cars']['kba_hsn'] . '-' . $this->data['cars']['kba_tsn'] ?></td>
											</tr>
											<tr>
												<td>Zustand</td>
												<td><?php 
													if ($this->data ['cars'] ['accident-damaged'] == 'false') echo "Unfallfrei";
													if ($this->data ['cars'] ['accident-damaged'] == 'true' &&  $this->data ['cars'] ['roadworthy']=="true") echo "Unfallschaden instandgesetzt";
													if ($this->data ['cars'] ['accident-damaged'] == 'true' &&  $this->data ['cars'] ['roadworthy']=="false") echo "Unfallschaden";
													?>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
								<!-- Spalten Div -->
							</div>
							<!-- tab res div -->
						</div>
						<!-- Panel Body -->
					</div>
					<!-- Panel Technihhce Daten -->
					<div class="panel panel-default shadow space">
						<!-- Beschreibung -->
						<div class="panel-heading">
							<h4 class="panel-title">Ausstattung</h4>
						</div>
						<div class="panel-body" id="Fahrzeugbeschreibung">
                               <?php
																															$desc = explode ( '*', $this->data ['cars'] ['enrichedDescription'] );
																															$spalten = array ();
																															for($a = 1; $a < count ( $desc ); $a ++) { // erste Zeil und die letzten 5 können weg
																																$spalten [fmod ( $a, 3 )] [] = str_replace ( "\\", "", $desc [$a] );
																															}
																															?>
                                    <?php foreach($spalten as $spalte){ ?>
                                    <div class="col-sm-4">
								<ul class="list-group">
	                                    <?php
																																					
foreach ( $spalte as $element ) {
																																						echo '<li class="list-group-item stripped">' . $element . '</li>';
																																					}
																																					?>                      
	                                    </ul>
							</div>
                                    <?php } ?>
                </div>
						<!-- Ausstattung Body -->
					</div>
					<!-- Panel Ausstattung -->
				</div>
				<!-- Panel Group -->
			</div>
		</div>
	</section>
	<!-- Section Fahrzeug komplett -->
	<!-- Footer -->
   <?php include 'static/foot_content.php'; ?>
 
      <!-- Modal Abgasnorm-->
	<div class="modal fade" id="Schadstoffklasse" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Info : Schadstoffklasse</h4>
				</div>
				<div class="modal-body">
					<p>Die Schadstoffklasse bestimmt die Steuerklasse eines Fahrzeugs.
						Sie finden sie unter Ziffer 14 (in der Zulassungsbescheinigung
						Teil 1).</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">zurück</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Verbrauch-->
	<div class="modal fade" id="Verbrauch" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Info : Verbrauch</h4>
				</div>
				<div class="modal-body">
					<p>Bei den angegebenen Daten handelt es sich um Circa-Angaben des
						Angebot-Erstellers. Die Werte können Erfahrungen zu diesem Modell
						darstellen oder aus anderen Quellen stammen.</p>
					<p>
						Weitere Informationen zum offiziellen Kraftstoffverbrauch und den
						offiziellen spezifischen CO2-Emissionen neuer PKW können dem
						"Leitfaden über den Kraftstoffverbrauch und die CO2-Emissionen
						neuer PKW" entnommen werden, der an allen Verkaufsstellen und bei
						der "Deutschen Automobil Treuhand GmbH" unentgeltlich erhältlich
						ist unter <a
							href="http://www.dat.de/angebote/verlagsprodukte/leitfaden-kraftstoffverbrauch.html">www.dat.de</a>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">zurück</button>
				</div>
			</div>
		</div>
	</div>

	<!-- Modal Mail Okay-->
	<div class="modal fade" id="mailstatus" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Ihre Anfrage</h4>
				</div>
				<div class="modal-body" id="msg"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">zurück</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Modal Social -->
		<?php
		// Links fuer Modal social
		$url = 'http://' . $_SERVER ['HTTP_HOST'] . $_SERVER ['PHP_SELF'] . '?' . $_SERVER ['QUERY_STRING'];
		$thisurl = urlencode ( $url );
		
		$shorttext = urlencode ( $this->data ['cars'] ['make_local_desc'] . ' ' . $this->data ['cars'] ['model_desc'] . ' Autohaus am Stadtpark Holderbaum, Kaiserslautern ' );
		
		$body = rawurlencode ( 'Hallo, Ich habe bei www.holderbaum.de ein Fahrzeug gesehen das Ihnen gefallen k�nnte.' . $this->data ['cars'] ['make_local_desc'] . ' ' . $this->data ['cars'] ['model_desc'] . ' Durch Klick auf den Link gelangen Sie direkt zum Inserat:' . $url . 'Viele Gr��e' );
		
		$twitter = ' href="https://twitter.com/home?status=' . $shorttext . $thisurl . '" ';
		$facebook = ' href="https://www.facebook.com/sharer/sharer.php?u=' . $thisurl . '" ';
		$pinterrest = ' href="https://pinterest.com/pin/create/button/?url' . $thisurl . '&media=' . $shorttext . '" ';
		$googlep = ' href="https://plus.google.com/share?url=' . $thisurl . '" ';
		$mailto = ' href="mailto:?subject=www.holderbaum.de%20-%20Fahrzeugempfehlung&body=' . $body . '" ';
		?>
	
		<div class="modal fade" id="social" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Fahrzeug teilen:</h4>
				</div>
				<div class="modal-body">
					<p>Gefällt Dir das Fahrzeug? Bitte teile es!</p>
					<div class="btn-group-vertical shadow">
						<a <?php echo $facebook ?> class="btn btn-default"><i
							class="fa fa-fw fa-facebook"></i> auf Facebook teilen</a> <a
							<?php echo $googlep ?> class="btn btn-default"><i
							class="fa fa-fw fa-google"></i>auf Google+ teilen</a> <a
							<?php echo $pinterrest ?> class="btn btn-default"><i
							class="fa fa-fw fa-pinterest"></i> pin in Pinterest </a> <a
							<?php echo $twitter ?> class="btn btn-default"><i
							class="fa fa-fw fa-twitter"></i> auf Twitter teilen</a> <a
							<?php echo $mailto ?> class="btn btn-default"><i
							class="fa fa-fw fa-share-alt"></i> per Mail weiterempfehlen</a>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">zurück</button>
				</div>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/jquery.validate.min.js"></script>
	<script src="bootstrap/js/jquery.form.min.js"></script>


	<script type="text/javascript">
    $(document).ready(function() {
     
        $('#<?php  echo $this->data['cars']['fin'] ?>').carousel();

		$('#response').append('<input type="hidden" name="check" id="check" value="<?php echo $this->data['timestamp']?>">');
		$('#response').append('<input type="hidden" name="ref" id="ref" value="<?php echo $this->data['cars']['fin']?>">');
		
		$(document).ajaxStart(function() {
			   $('#submit span').toggleClass('hidden');
			});

		$(document).ajaxSuccess(function() {
				$('#submit span').toggleClass('hidden');
			});
		
        $('#response').validate({
        	submitHandler: function(form) {
        		$.ajax({url: "index.php?" + $('#response').formSerialize(), success: function(result){
        			console.log(result);
        			var erg = jQuery.parseJSON(result);
        			document.getElementById('msg').innerHTML ='<div class="text-center alert ' + erg.status +'"><strong>' + erg.msg + '</strong></div>';
        			$('#response').clearForm();
        	        $('#check').val(erg.hash);
        	        $('#mailstatus').modal({show:true});
        	        }});
              	  }
            });
          });
    </script>
</body>

</html>
