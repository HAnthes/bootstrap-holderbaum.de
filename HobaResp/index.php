<?php
/**
 * MVC Model
 * @version 1
 */

function __autoload($class_name){
		
	include_once 'lib' . DIRECTORY_SEPARATOR .$class_name . '.php';
}

// Parameter sammeln
$request = array_merge($_GET, $_POST);

//Controller instanzieren
$controller = new mvc_Controller($request);

//Ausgabe Anzeigen
echo $controller->display();

?>