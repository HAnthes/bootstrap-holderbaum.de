<?php
/*
 * Holt Json Kontakte und bietet verschiedene getter
 * Parameter baseUrl - fehlt noch ein Konfiguration (auch f�r die SearchAPI)
 */
class contactAPI{
	private $contacts = array();
	private $baseUrl = '';
	
	public function __construct() {
		$conf = new _Config();
		$this->baseUrl = $conf->getContactsUrl();
		$this->contacts = json_decode(file_get_contents($this->baseUrl));
	}
			
	public function getAll(){
		return $this->contacts;
	}
	
	public function getByOrg($org){
		return array_filter($this->contacts, function($v) use ($org){return $v->org == $org;});
	}
	
	public function getByPos($pos){
		return array_filter($this->contacts, function($v) use ($pos){return $v->pos == $pos;});
	}
	
	public function getByName($name){
		return array_filter($this->contacts, function($v) use ($name){return $v->name == $name;});
	}
	
	public function getByID($id){
		return array_filter($this->contacts, function($v) use ($id){return $v->id == $id;});
	}
	
	public function getByShopID($shopid){
		return array_filter($this->contacts, function($v) use ($shopid){return $v->shopid == $shopid;});
	}
	
}
?>
