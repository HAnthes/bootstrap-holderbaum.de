<?php
/*
 * Mailerklasse
 * Versendet Mails. Als Empf�nger Mails aus der der "Contact Api"
 */
class Mailer{
	
	private $sender;
	private $empfaenger;
	private $nachricht;
	private $betreff;
	
	public function setMail($sender, $betreff, $nachricht){
		$this->sender = $sender;
		$this->betreff=$betreff;
		$this->nachricht=$nachricht;
	}
	
	private function defaulttarget(){
		$this->empfaenger = $contact->getByName('default');
	}
	
	/**
	 * Setzt Empf�nger - bei Fehler wird ein Defaultwert gesetzt
	 * @param unknown $kontaktname
	 */
	public function setEmpfaenger($kontaktname){
		$contact = new contactAPI();
		
		//Nur Buchstaben sonst default!
		if (isset($kontaktname) && preg_match('[^A-Za-z]', $kontaktname, $matches) != 0){
			$this->defaulttarget();
		}
		
		$this->empfaenger = $contact->getByName($kontaktname);
		
		//Fallback falls kein g�ltiger Nanem gegeben
		if(!isset($this->empfaenger)){
			$this->defaulttarget();
		}
		
	}

	public function send(){
	
		if(isset($this->sender) && isset($this->empfaenger) && isset($this->nachricht) && isset($this->betreff)){
			
			$tempempf = $this->empfaenger[key($this->empfaenger)]->mail;
			
			$header = 'From: ' . $this->sender . "\r\n" .
   						 'X-Mailer: m/' . '1';
			$betreff=$this->betreff;
			$nachricht=$this->nachricht;
			
			
			return mail($tempempf, $betreff, $nachricht, $header);
		} else {
			return false;
		}
	}
	
	
}

?>