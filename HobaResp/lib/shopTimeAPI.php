<?php
/*
 * Holt Json Öffnungszeiten
 * 
 */
class shopTimeAPI{
	private $shoptimes = array();
	private $baseUrl = '';
	
	public function __construct() {
		$conf = new _Config();
		$this->baseUrl = $conf->getShoptimeURL();
		$this->shoptimes = json_decode(file_get_contents($this->baseUrl));
	}
			
	public function getAll(){
		return $this->shoptimes;
	}
	
	public function getByShop($shop){
		return array_filter($this->shoptimes, function($v) use ($shop){return $v->shop == $shop;});
	}
	
	public function getByID($id){
		return array_filter($this->shoptimes, function($v) use ($id){return $v->id == $id;});
	}
	
}
?>
