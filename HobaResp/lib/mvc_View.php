<?php
/**
 * View Klasse
 * Zuweisen der Daten
 * Laden des Templates
 * R�ckgabe der "Ausgabe"
 * 
 * @version 1
 * @todo path in Configuration auslagern 
 * @author anthes
 */
class mvc_View{
	
	//Templatepfad
	private $path = 'templates';
	
	//Template
	private $template;
	
	//Daten f�r Template
	private $data = array();
	
	//Schl�ssel Var zordnung
	function assign($key, $value){
		$this->data[$key] = $value;
	}
	
	function setTemplate($template){
		$this->template = $template;
	}
	
	function loadTemplate(){
		$file = $this->path . DIRECTORY_SEPARATOR . $this->template . ".php";
		if (file_exists($file)){
			ob_start();
			include $file;
			$output = ob_get_contents();
			ob_end_clean();
			return $output;
		}
		else {
			return 'Fehler!';
		}
	}

}
?>