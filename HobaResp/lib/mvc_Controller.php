<?php
/**
 * Controller Klasse
 * @author anthes
 * 
 * Template �ber view Parameter
 * 
 * todo : Trennung in inneres und �u�eres Template
 */

class mvc_Controller{
	
	private $request = null;
	private $template = '';
	private $view = null;
	
	
	private $bas64TimeStamp='';
	
	
	/*
	 * Default Template festlegen
	 */
	public function __construct($request){
		
		$this->view = new mvc_View();
		$this->request = $request;
		$this->template = !empty($request['view']) ? $request['view'] : 'Service';
		
		/*F�r Spamschutz im Formular */
		$timestamp = time();
		$keystring = 'aggrokurt';
		$hash = crypt($timestamp . $keystring);
		$this->bas64TimeStamp = base64_encode($timestamp .';' . $keystring . ';' . $hash);
	}
	
	public function display(){
		switch($this->template){
			//AD: FZ Detailseite
			case 'ad' :
				$this->adTemplate();
				break;
			//Default noch FZ �bersichtsseite!		
			case 'list':
				$this->listTemplate();
				break;
			case 'counter':  //Ajax : Anzahl FZ nach Filter
				$this->counterTemplate();
				break;
			case 'jform':  //Ajax : formular
				$this->formTemplate();
				break;
			case 'Impressum':
				$this->Impressum();
				break;
			case 'call':
				$this->MailRedirect();
				break;
			case 'Datenschutz':
				$this->Datenschutz();
				break;
			case 'USKunden':
				$this->kontakt('USKunden');
				break;
			case 'Kontakt':
				$this->kontakt('Kontakt');
				break;
			case 'Service':
					$this->kontakt('Service');
					break;
			default:
				$this->view->setTemplate('default');
		}
		
		return $this->view->loadTemplate();
	}
	
	
	/*
	 * Eine Anzeige darstellen - Mailfunktion!
	 * Bei Fehlern direkt Liste Laden.
	 */
	private function adTemplate(){
		if(isset($this->request['ad'])){
						$searchAPI = new SearchAPI();
						if( preg_match('/[^0-9]/', $this->request['ad'], $matches) == 0){
							$this->view->setTemplate('searchAPIad');
							$this->view->assign('cars', $searchAPI->getCar($this->request['ad']));
							$this->view->assign('ad', $this->request['ad']);
							$this->view->assign('timestamp', $this->bas64TimeStamp);
						}
						} else {
							listTemplate();
					}
	}
	
	
	/*
	 * Erzeugen der Fahrzeugliste mit Seitenschaltung und Filter.
	 * make		= Liste m�glicher Hersteller
	 * cat		= Liste m�glicher Aufbauten
	 * fuel		= Treibstoffe
	 * clima	= Kimaarten
	 * filters 	= Filterparameter f�r GetMethode (Seitenlnk)
	 * car 		= Liste von Fahrzeugen
	 * titel	= �berschrift/Titel 
	 * todo : redundater Code f�r die Checkboxes
	 */
	private function listTemplate(){
				
				$searchAPI = new SearchAPI();
		
				//Template f�r die Ausgabe festlegen
				$this->view->setTemplate('searchAPIList');
		
				//Rest Query ausgeben - Debug, schreibt mit echo die Mobileanfrage.
				//$this->searchAPI->setViewQuery();
				
				
				//Array mit Filterparametern aus dem dann der Get Link f�r die Seitenschaltung zusammengesetzt wird
				$getLink = array();
					
				//Formular Hersteller auswerten und Daten f�r Formular erstellen 
				$filterArray = array ();
				foreach ( $searchAPI->getMakeList() as $element ){
					if(isset($this->request['make']) && in_array($element, $this->request['make'])){
						$searchAPI->setMake($element);
						$temp = array('name'=>$element, 'value'=>'checked');
						$getLink[] = 'make[]=' . $element;
					} else{
						$temp = array('name'=>$element, 'value'=>'');
					}
					//View Variabele zusammensetzen
					$filterArray[]=$temp;
				}
				$this->view->assign('make', $filterArray);
				
				//Formular Kategorie auswerten und Daten f�r Formular erstellen
				//Deutsche texte f�r Icon's
				//Todo : Auswertung der mobile Daten f�r diese F�lle.
				$filterArray = array ();
				foreach ( $searchAPI->getCategoryList() as $element ){
					$temp= array();
					switch($element){
						case 'Cabrio' :
							$temp['key'] = 'Cabrio';
							break;
						case 'Limousine':
							$temp['key'] = 'Limousine';
							break;
						case 'SmallCar':
							$temp['key'] = 'Kleinwagen';
							break;
						case 'EstateCar':
							$temp['key'] = 'Kombi';
							break;
						case 'OffRoad':
							$temp['key'] = 'Gel&auml;ndewagen';
							break;
						case 'SportsCar':
							$temp['key'] = 'Coup&eacute;';
							break;
						case 'Van':
							$temp['key'] = 'Van';
							break;
					}	
					if(isset($this->request['cat']) && in_array($element, $this->request['cat'])){
						$searchAPI->setCategory($element);
						$temp['name'] =$element;
						$temp['value']= 'checked';
						$getLink[] = 'cat[]=' . $element;
					} else{
						$temp['name'] =$element;
						$temp['value']= '';
					}
					$filterArray[]=$temp;
				}
				$this->view->assign('cat', $filterArray);
				
				//Formular Treibstoff auswerten und Daten f�r Formular erstellen
				$filterArray = array ();
				foreach ( $searchAPI->getFuelList() as $element ){
					if(isset($this->request['fuel']) && in_array($element['key'], $this->request['fuel'])){
						$searchAPI->setFuel($element['key']);
						$temp = array('name'=>$element['key'], 'desc'=>$element['desc'], 'value'=>'checked');
						$getLink[] = 'fuel[]=' . $element['key'];
					} else{
						$temp = array('name'=>$element['key'], 'desc'=>$element['desc'], 'value'=>'');
					}
					$filterArray[]=$temp;
				}
				$this->view->assign('fuel', $filterArray);
				
				//Formular Klima auswerten und Daten f�r Formular erstellen
				$filterArray = array ();
				foreach ( $searchAPI->getClimaList() as $element ){
					if(isset($this->request['clima']) && in_array($element['key'], $this->request['clima'])){
						$searchAPI->setClimatisation($element['key']);
						$temp = array('name'=>$element['key'], 'desc'=>$element['desc'], 'value'=>'checked');
						$getLink[] = 'clima[]=' . $element['key'];
					} else{
						$temp = array('name'=>$element['key'], 'desc'=>$element['desc'], 'value'=>'');
					}
					$filterArray[]=$temp;
				}
				$this->view->assign('clima', $filterArray);
				
				
				/*
				 * Seitenschaltung sichern
				 * Bei Sortierungen und Filter wird keine Seite gesetzt - im Link.
				 */
				if(isset($this->request['page'])){
					if( preg_match('/[^0-9]/', $this->request['page'], $matches) == 0){
						$searchAPI->setPage($this->request['page']);
					 }
                }
                
                /*
                 * Seiteschaltung - Get Methode mit den Filtereinstellungen
                 */
              	$temp="";
				foreach($getLink as $element) $temp .= '&'. $element; 
				$this->view->assign('filters', $temp );

				
				/*Fahrzeug holen*/
				$this->view->assign('cars', $searchAPI->getCars());
				$this->view->assign('titel', 'Fahrzeugeliste');
	}
	
	
	/*
	 * Ermittelt die Anzahl der selektierten Fahrzeuge durch den akutelle Filter.
 	 * todo : redundater Code f�r die Checkboxes
	 */
	private function counterTemplate(){
		$searchAPI = new SearchAPI();
		//Template f�r die Ausgabe festlegen
		$this->view->setTemplate('ajax_searchAPIcounter');
		
		//Rest Query ausgeben - Debug
		//$this->searchAPI->setViewQuery();
		
		if(isset($this->request['make'])){
			foreach ($this->request['make'] as $element){
				$searchAPI->setMake($element);
			}
		}
		
		if(isset($this->request['cat'])){
			foreach ($this->request['cat'] as $element){
				$searchAPI->setCategory($element);
			}
		}
		
		if(isset($this->request['clima'])){
			foreach ($this->request['clima'] as $element){
				$searchAPI->setClimatisation($element);
			}
		}
		
		if(isset($this->request['fuel'])){
			foreach ($this->request['fuel'] as $element){
				$searchAPI->setFuel($element);
			}
		}
		
		
		$temp = $searchAPI->getCars();
		$this->view->assign('counter', $temp['total']);	
	}
	
	/*
	 * Formular auswerten und Valdieren.
	 */
	private function formTemplate(){

		$isokay = 0; //Bei Fehlern hochz�hlen
		$errorMSG="";
		$empfaenger="default";
		
		//Passt der Request von den Zeichen
		if ( isset($this->request['check']) && preg_match('/[^0-9A-Za-z=]/', $this->request['check'], $matches) == 0){
			$check = explode(';',base64_decode($this->request['check']));
		} else {
			$isokay++;
			$errorMSG .="Fehlerhafte Zeichen in der Antwort. ";
		}
			
		//Array muss drei Elemente haben
		$stopTime = time();
		if($isokay==0 && count($check)==3){
			$startTime = $check[0];  	
		} else {
			$isokay++;
			$errorMSG .="Array nicht vollst�ndig. ";
		}
		
		//Isset zum vermeiden von Fehler bei der Var Pr�fung kurzschlusslogik
		
		//Ist Starttime ein Integer
		if(isset($startTime) && !gettype($startTime)=='integer'){ 
			$isokay++; //Type Pr�fen
			$errorMSG .="Falscher Zeittype. ";
		}
		
		//ref = Fin ist vorhanden
		if ( (isset($this->request['ref'])) && (preg_match('/http:|www.|<a|>/', $this->request['ref'], $matches) != 0) && (strlen($this->request['ref']<5))) {
			$isokay++;
			$errorMSG .="Fehler beim Referenz. ";
		}
			
		//Textfeld nicht leer.
		if(isset($this->request['messagebox']) && (strlen($this->request['messagebox'])<2) ) {
			$isokay++;
			$errorMSG .="Textarea leer. " ;
		}
		
		//regex http:|www.|<a|> Span inhalte.... erste Stufe, Testdaten m�ssen noch ausgewertet werden.
		if ( isset($this->request['messagebox']) && preg_match('/http:|www.|<a|>/', $this->request['messagebox'], $matches) != 0){
			$isokay++;
			$errorMSG .="Regex hat HTML gefunden. ";
		}
		
		//Email pr�fen - ist vorhanden
		//email
		if(isset($this->request['email']) && !filter_var($this->request['email'], FILTER_VALIDATE_EMAIL)){
			$isokay++;
			$errorMSG .="Keine Mailadresse. " ;
		}
		
		
		//vemail pr�fen - ist leer.
		if(!isset($this->request['vemail']) ) {
			$isokay++;
			$errorMSG .="V-Email gesetzt. " ;
		}
		//Telefon pr�fen kein schei� drinn 
		if ( (isset($this->request['phone'])) && (preg_match('/http:|www.|<a|>/', $this->request['phone'], $matches)!=0)) {
			$isokay++;
			$errorMSG .="Regexger�se bei Telefon. ";
		}
		
		//Zeit differenz zwischen 30 Sekunde und max 10 Minuten 600sek.
		if(isset($startTime) &&  $stopTime - $startTime<=15) {
			$isokay++;
			$errorMSG .="Antwort zu schnell. ";
		}
		
		//Zeit differenz zwischen 30 Sekunde und max 10 Minuten 600sek.
		if(isset($startTime) && $stopTime - $startTime>=600){
			$isokay++;
			$errorMSG .="Antwort timeout. ";
		}
	
		
		$this->view->setTemplate('ajax_contact');
		if ($isokay==0){
			
			//Holen Kontakt, Anh�ngen Nachricht, Header versenden - okay oder Fehler auswerten.
			$mailer = new Mailer();
			if(isset($this->request['phone'])){
					$tempnachricht = "Telefon : " . $this->request['phone'] . '\r\n \r\n' . $this->request['messagebox'];
			} else {
				$tempnachricht = $this->request['messagebox'];
			}
			
			$mailer->setMail($this->request['email'], 'Anfrage : ' .$this->request['ref'], $tempnachricht);
			$mailer->setEmpfaenger('Verkauf');
			
			if ($mailer->send()){
				$this->view->assign('status','alert-success' );
				$this->view->assign('msg','Ihre Anfrage wurde erfolgreich verschickt');			
			} else {
				$this->view->assign('status','alert-danger' );
				$this->view->assign('msg', 'Fehler beim Versenden der Nachricht');
			}
						
		}
		else{			
			$this->view->assign('status','alert-danger' );
			//$this->view->assign('msg', $errorMSG); f�r Debug im Echtbetrieb feste Antwort
			$this->view->assign('msg', 'Fehler beim Versenden der Nachricht!');
		}
		$this->view->assign('hash',$this->bas64TimeStamp );
	
	}
	
	/*
	 * Redirect View.
	 * Chorme f�rh den redirekt direkt aus.
	 * FF zeigt die Seite an. 
	 * Info wer eine mail bekommt und js Button zur�ck
	 */
	private function MailRedirect(){
		$contactAPI = new contactAPI();
		$error=false;
		$c=null;
		if (!empty($this->request['id'])) {
			$id=$this->request['id'];
			$c = $contactAPI->getByID($id);
			if (empty($c)){
				$c=$contactAPI->getByName('Hallo Holderbaum');
				$error = true;
			}
		} else {
			$c = $contactAPI->getByName('Hallo Holderbaum');
			$error=true;
		}
		$this->view->setTemplate('Call');
		$this->view->assign('gf', $c);
		$this->view->assign('error', $error);
	}
	
	/*
	 * Einfachere Seiten 
	 */
	private function Impressum(){
		$contactAPI = new contactAPI();
		$gf = $contactAPI->getByPos('gf'); //GF - Gesch�ftf�hrer
		$this->view->setTemplate('Impressum');
		$this->view->assign('gf', $gf);
	}
	
	private function Datenschutz(){
		$contactAPI = new contactAPI();
		$ds = $contactAPI->getByPos('ds'); //Datenschutz
		$this->view->setTemplate('Datenschutz');
		$this->view->assign('gf', $ds);
	}
	
	//Kontakt Template - alle Kontakte unterteilt in Service Verkauf und Teile (z.b. USKunden oder einfachter Kontakt
	private function kontakt($template){
		$contactAPI = new contactAPI();
		$shopTimeAPI = new shopTimeAPI();
		$searchAPI = new SearchAPI();
		
		$searchAPI->setImageCount(4, 20);
		$this->view->assign('cars', $searchAPI->getCars());
		
		//Kontakte Service, Teile, VK + �ffnungszeiten
		$service = array();
		$service['kontakt'] = $contactAPI->getbyName('Service');
		$service['zeiten'] = $shopTimeAPI->getbyShop('Service');
		$service['employes'] = $contactAPI->getByOrg('Service');
		
		$verkauf = array();
		$verkauf['kontakt'] = $contactAPI->getbyName('Verkauf');
		$verkauf['zeiten'] = $shopTimeAPI->getbyShop('Verkauf');
		$verkauf['employes'] = $contactAPI->getByOrg('Verkauf');
		
		$teile = array();
		$teile['kontakt'] = $contactAPI->getbyName('Teileservice');
		$teile['zeiten'] = $shopTimeAPI->getbyShop('Teileservice');
		$teile['employes'] = $contactAPI->getByOrg('Teile');
		
		
		$this->view->setTemplate($template);
		$this->view->assign('service', $service);
		$this->view->assign('verkauf', $verkauf);
		$this->view->assign('teile', $teile);
		
	}
		
}
?>