<?php

/**
 * Mobile Search API
 * 
 * Stellt Array mit Fahrzeugen zusammen
 * beim Erzeugen erst Abfrage f�r Globale Information Marken, Modelle usw.
 * Getter und Setter f�r Filter und Suche - bis jetzt nur Marken und Category umgesetzt
 * Abfrage Einzelanzeige.
 * 
 * @version 1
 * @author anthes
 *
 */


class SearchAPI {
	
	private $url, $username, $password, $language;
	
	private $xml;
	
	private $rawdata;
	
	private $maxPage=1;
	
	private $seachparameter = array();
	
	private $viewquery=0;

	/**
	 * erster Request um Daten f�r die Auswahl in der View zu sammeln
	 */
	public function __construct() {
		
		$conf = new _Config();
		$user = $conf->getUser();
		$this->username = $user['name'];
		$this->password = $user['pw'];
		
		$this->language = 'de';
		$this->url = 'https://services.mobile.de/';
		//Starteinstellungen
		$this->seachparameter['page.size='] = '20';
		$this->seachparameter['page.number='] = '1';
		$this->seachparameter['classification='] = '';
		$this->sendRequest ( 'search-api/search?page.size=100' );
	}
	
	/*
	 * Debug einschalten
	 */
	function setViewQuery(){
		$this->viewquery=1;
	}
	
	/*
	 * API Abruf 
	 * http://services.mobile.de/manual/search-api.html
	 */
	private function sendRequest($query) {
		if ($this->viewquery==1) print("Query" . ": " . $query);
		$result = "";
		$b64 = base64_encode ( $this->username . ':' . $this->password );
		$auth = "Authorization: Basic $b64";
		$opts = array (
				'http' => array (
						'method' => "GET",
						'header' => $auth . "\r\n" . 'User-Agent: v2' . "\r\nAccept-Language: " . $this->language
				)
		);
	
		$context = stream_context_create ( $opts );
		$fp = @fopen ( $this->url . $query, 'r', false, $context );
		if ($fp != NULL) {
		
			while ( $str = fread ( $fp, 1024 ) ) {
				$result .= $str;
			}
			fclose ( $fp );

			$this->xml = new SimpleXMLElement ( $result );
			$this->rawdata = $result;
		} else {
			return 'Connection Error: could not connect to ' . $this->url;
		}
	}
	
	/*
	 * XPath Methode zum zerlegen
	 */
	private function simpleQuerry($query,$key) {
		$temp = array ();
		
		foreach ( $this->xml->xpath( $query ) as $re ) {
			$temp[] = (String)$re[0][$key];
		}
		return array_unique ( $temp );
	}
	
	
	/*
	*XML vom reqeust
	*/
	function getRawdata(){
			return $this->rawdata;
		}
	/*
	 * Fahrzeuge ueber die Such API 
	 */
	function getCars() {
		$cars = array();
		
		$parameters ='?';
		foreach($this->seachparameter as $key => $parameter){
			$parameters .=  $key . $parameter . "&";
		}
		
		$this->sendRequest ( 'search-api/search' . $parameters );
			
		//Seiten Informatinen
		$cars["seite"] = (String)$this->xml->xpath('/search:search-result/search:current-page')[0];
		$this->maxPage = (String)$this->xml->xpath('/search:search-result/search:max-pages')[0];
	
		$cars["max"] = $this->maxPage;
		$cars["total"] = (String)$this->xml->xpath('/search:search-result/search:total')[0];
	
		$temp = array();
		foreach($this->xml->xpath('/search:search-result/search:ads/ad:ad') as $item) {	
				$key = (string)$item['key'];
				$temp[$key] = $this->getData($item);
			}
		$cars['cars'] = $temp;
		
		return $cars;	
	}
	
	/*
	 * Ein Fahrzeug ueber ID
	 */
	function getCar($adId){
		
		$this->sendRequest('search-api/ad/' . $adId);
		$car = array();
		$car = $this->getData($this->xml);
		return $car;
		
	}

	/*
	 * XML Daten in Array
	 */
	private function getData($item){
				$buffer = array();
				
				//Preis Informationen
				$pref = 'ad:price/ad:';
				$buffer['preis']   = $this->issValue($item->xpath($pref . 'consumer-price-amount'),'value');
				$buffer['vatable'] = $this->issValue($item->xpath($pref . 'vatable'),'value');
				$buffer['vatrate'] = $this->issValue($item->xpath($pref . 'vat-rate'),'value');
				
				$pref = 'ad:vehicle/ad:';
				$buffer['category_key'] 		= $this->issValue($item->xpath($pref . 'category'),'key');
				$buffer['category_local_desc']	= $this->issValue($item->xpath($pref . 'category/resource:local-description'),'');
				$buffer['make_key'] 			= $this->issValue($item->xpath($pref . 'make'),'key');
				$buffer['make_local_desc']		= $this->issValue($item->xpath($pref . 'make/resource:local-description'),'');
				$buffer['model_key'] 			= $this->issValue($item->xpath($pref . 'model'),'key');
				$buffer['model_local_desc']		= $this->issValue($item->xpath($pref . 'model/resource:local-description'),'');
				$buffer['model_desc'] 			= $this->issValue($item->xpath($pref . 'model-description'),'value');
				
				$buffer['damage-and-unrepaired']		= $this->issValue($item->xpath($pref . 'damage-and-unrepaired'),'value');
				$buffer['accident-damaged']				= $this->issValue($item->xpath($pref . 'accident-damaged'),'value');
				$buffer['roadworthy']					= $this->issValue($item->xpath($pref . 'roadworthy'),'value');
				
				//Features
				$temp = array();
				foreach($item->xpath($pref . 'features/ad:feature') as $feature){
					$temp[(String)$feature['key']] =(String)$feature->xpath('resource:local-description')[0];
				}
				$buffer['features'] = $temp;
				
				$pref ='ad:vehicle/ad:specifics/ad:';
				$buffer['exterior-color_key'] 			= $this->issValue($item->xpath($pref . 'exterior-color'),'key');
				$buffer['exterior-color_local_desc']	= $this->issValue($item->xpath($pref . 'exterior-color/resource:local-description'),'');
				$buffer['exterior-color_name'] 			= $this->issValue($item->xpath($pref . 'exterior-color/ad:manufacturer-color-name'),'value');
				$buffer['mileage']						= $this->issValue($item->xpath($pref . 'mileage'),'value');
				$buffer['general-inspection']			= $this->issValue($item->xpath($pref . 'general-inspection'),'value');
				$buffer['door_key']						= $this->issValue($item->xpath($pref . 'door-count'),'key');
				$buffer['door_desc']					= $this->issValue($item->xpath($pref . 'door-count/resource:local-description'),'');
				$buffer['first-registration']			= $this->issValue($item->xpath($pref . 'first-registration'),'value');
				
				$buffer['emission-class_key']			= $this->issValue($item->xpath($pref . 'emission-class'),'key');
				$buffer['emission-class_local_desc']	= $this->issValue($item->xpath($pref . 'emission-class/resource:local-description'),'');
				
				$buffer['emission-envkv_co2']			= $this->issValue($item->xpath($pref . 'emission-fuel-consumption'),'co2-emission');
				$buffer['emission-envkv_inner']			= $this->issValue($item->xpath($pref . 'emission-fuel-consumption'),'inner');
				$buffer['emission-envkv_outer']			= $this->issValue($item->xpath($pref . 'emission-fuel-consumption'),'outer');
				$buffer['emission-envkv_combined']		= $this->issValue($item->xpath($pref . 'emission-fuel-consumption'),'combined');
				$buffer['emission-envkv_fuel']			= $this->issValue($item->xpath($pref . 'emission-fuel-consumption'),'petrol-type');
				$buffer['emission-sticker_key']			= $this->issValue($item->xpath($pref . 'emission-sticker'),'key');
				$buffer['emission-sticker_local_desc']	= $this->issValue($item->xpath($pref . 'emission-sticker/resource:local-description'),'');
				
				$buffer['fuel_key']						= $this->issValue($item->xpath($pref . 'fuel'),'key');
				$buffer['fuel_local_desc']				= $this->issValue($item->xpath($pref . 'fuel/resource:local-description'),'');
				$buffer['power']						= $this->issValue($item->xpath($pref . 'power'),'value');
				$buffer['kba_hsn']						= $this->issValue($item->xpath($pref . 'kba'),'hsn');
				$buffer['kba_tsn']						= $this->issValue($item->xpath($pref . 'kba'),'tsn');
		
				$buffer['gearbox_key']					= $this->issValue($item->xpath($pref . 'gearbox'),'key');
				$buffer['gearbox_local_desc']			= $this->issValue($item->xpath($pref . 'gearbox/resource:local-description'),'');
				
				$buffer['climatisation_key']			= $this->issValue($item->xpath($pref . 'climatisation'),'key');
				$buffer['climatisation_local_desc']		= $this->issValue($item->xpath($pref . 'climatisation/resource:local-description'),'');
				
				$buffer['num-seats']					= $this->issValue($item->xpath($pref . 'num-seats'),'value');
				$buffer['cubic-capacity']				= $this->issValue($item->xpath($pref . 'cubic-capacity'),'value');
				$buffer['condition_key']				= $this->issValue($item->xpath($pref . 'condition'),'key');
				$buffer['condition_local_desc']			= $this->issValue($item->xpath($pref . 'condition/resource:local-description'),'');
				
				$buffer['fin']							= $this->issValue($item->xpath($pref . 'identification-number'),'value');
				
				$buffer['interior-color_key']			= $this->issValue($item->xpath($pref . 'interior-color'),'key');
				$buffer['interior-color_local_desc']	= $this->issValue($item->xpath($pref . 'interior-color/resource:local-description'),'');
				
				$buffer['interior-type_key']			= $this->issValue($item->xpath($pref . 'interior-type'),'key');
				$buffer['interior-type_local_desc']		= $this->issValue($item->xpath($pref . 'interior-type/resource:local-description'),'');
				
				$buffer['airbag_key']					= $this->issValue($item->xpath($pref . 'airbag'),'key');
				$buffer['airbag_local_desc']			= $this->issValue($item->xpath($pref . 'airbag/resource:local-description'),'');
				
				$buffer['previous-owners']				= $this->issValue($item->xpath($pref . 'number-of-previous-owners'),'');
				
				
				
				
				//Parksenoren
				$temp = array();
				foreach ($item->xpath($pref . 'parking-assistants/ad:parking-assistant') as $sensors){
					//https://services.mobile.de/refdata/parkingassistants/ ...net so wirklich schoen :(
						$help = (String)$sensors[0]['key'];
						if ($help=='FRONT_SENSORS') {
							$sensor = 'Front';
						}
						if ($help=='AUTOMATIC_PARKING'){
							$sensor = 'Selbstlenksystem';
						}
						if ($help=='REAR_SENSORS') {
							$sensor = 'Heck';
						}
						if ($help=='REAR_VIEW_CAM') {
							$sensor = 'R�ckfahrkamera';
						}
						$temp[$help] = $sensor;
				}
				$buffer['parkingassistants'] = $temp;
				
				
				
				//Such Ad - 1 Bilder, Einzel mehrere Bilder.
				$pref ='ad:images/ad:image';
				$temp = array();
				foreach($item->xpath($pref) as $bilder){
					$b = array();
					foreach($bilder->xpath('ad:representation') as $bild){
						$b[(String)$bild['size']] = (String)$bild['url'];
					}
					$temp[] = $b;
				}
			$buffer['bilder'] = $temp;
			
			$pref = 'ad:';
			$buffer['description']				= $this->issValue($item->xpath($pref . 'description'),'');
			$buffer['enrichedDescription']		= $this->issValue($item->xpath($pref . 'enrichedDescription'),'');
			
			return $buffer;
	}
	
	/**
	* Hilfmethode zum zerlegen
	* Fuer den Fall das ein Wert nicht vorhanden ist sichegestellt das ein "" zurueck kommt
	**/
	private function issValue($x, $v){
		$r = "";
		if ( isset($x[0]) ){
			if ($v!=""){
			$r = (String)$x[0][$v];
			} else {
				$r = (String)$x[0];
			}
		}
		return $r;
	}
	
	/**
	* Setter fuer die Such und Seitenparameter
	**/
	
	public function setPage($page){
		if( preg_match('/[^0-9]/', $page, $matches) == 0){
			$this->seachparameter['page.number='] = $page;
		}
	}
	
	/*
	 * Getter und Setter f�r Sortierung
	 */
	function getSort(){
		$temp['keys'] = array(
				array ('key'=>'price','desc'=>'Preis'),
				array('key'=>'mileage', 'desc'=>'Kilometer'),
				array('key'=>'first-registration', 'desc'=>'Erstzulassung'),
	
		);
		$temp['order'] = array ( 'up' => 'Aufw�rts', 'down' => 'Abw�rts');
		return $temp;
	}
	
	function setSort($field, $order){
		$this->seachparameter['sort.field'] = $field;
		$this->seachparameter['sort.order'] = $order;
	}
	
	/*
	 * Fuer die Filter Marke und Kategorie
	 */
	function getMakeList() {
		return $this->simpleQuerry ( '//ad:make', 'key' );
	}
	
	function setMake($make){
		if( preg_match('/[^a-zA-Z��� ]/', $make, $matches) == 0 && count($make)<16){
			$temp = 'refdata/classes/Car/makes/'.$make;
			if($this->seachparameter['classification=']==''){
				$this->seachparameter['classification=']  = $temp;
			} else
			{
				$this->seachparameter['classification=']  .= ','. $temp;
			}
		}
	}
	
	function getCategoryList() {
		return $this->simpleQuerry ( '//ad:category', 'key' );
	}
	
	function setCategory($cat){
		if( preg_match('/[^a-zA-Z ]/', $cat, $matches) == 0 && count($cat)<10){
			if(!isset($this->seachparameter['category='])){
				$this->seachparameter['category=']  = $cat;
			} else
			{
				$this->seachparameter['category=']  .= ','. $cat;
			}
		}
	}
	
	/*
	 * todo : Ermitteln aus den Datenvorhandener Fahrzeuge
	 * Sprache
	 * https://services.mobile.de/refdata/fuels
	 */
	function getFuelList(){
		return array(
				array('key'=>'PETROL', 'desc'=>'Benzin'),
				array('key'=>'DIESEL', 'desc'=>'Diesel'),
				array('key'=>'LPG', 'desc'=>'LPG'),
				array('key'=>'CNG', 'desc'=>'CNG')
		);
	
	}
	
	function setFuel($cat){
		if( preg_match('/[^A-Z]/', $cat, $matches) == 0 && count($cat)<6){
			if(!isset($this->seachparameter['fuel='])){
				$this->seachparameter['fuel=']  = $cat;
			} else
			{
				$this->seachparameter['fuel=']  .= ','. $cat;
			}
		}
	}
	
	/*
	 * todo : wie Fuel
	 * Sprache
	 * https://services.mobile.de/refdata/climatisations
	 *
	 */
	function getClimaList(){
		return array(
				array('key'=>'NO_CLIMATISATION', 'desc'=>'ohne'),
				array('key'=>'MANUAL_CLIMATISATION', 'desc'=>'Klimaanlage'),
				array('key'=>'AUTOMATIC_CLIMATISATION', 'desc'=>'Klimaautomtik')
		);
	}
	
	function setClimatisation($cat){
		if( preg_match('/[^A-Z_]/', $cat, $matches) == 0 && count($cat)<30){
			if(!isset($this->seachparameter['climatisation='])){
				$this->seachparameter['climatisation=']  = $cat;
			} else
			{
				$this->seachparameter['climatisation=']  .= ','. $cat;
			}
		}
	}
	
	/*
	 * Bilder -> Frontseite nur Ad's mit Bildern
	 */
	function setImageCount($min, $max){
		
		$this->seachparameter['imageCount.min='] = $min;
		$this->seachparameter['imageCount.max='] = $max;
	
	}
	
	
}


?>