    (function ($) {
        $.fn.fullscreenr = function (options) {

            var options = $.extend({}, options);
            $(document).ready(function () {
                $(options.bgID).fullscreenrResizer(options);
            });
            $(window).bind("resize", function () {
                $(options.bgID).fullscreenrResizer(options);
            });
            return this;
        };
        $.fn.fullscreenrResizer = function (options) {
            // Set bg size
            var ratio = options.height / options.width;
            // Get browser window size
            var browserwidth = $(window).width();
            var browserheight = $(window).height();
            // Scale the image
            if ((browserheight / browserwidth) > ratio) {
                $(this).height(browserheight);
                $(this).width(browserheight / ratio);
            } else {
                $(this).width(browserwidth);
                $(this).height(browserwidth * ratio);
            }
            // Center the image
            $(this).css('left', (browserwidth - $(this).width()) / 2);
            $(this).css('top', (browserheight - $(this).height()) / 2);
            return this;
        };
    })(jQuery);

  $.fn.fullscreenr({
        width: 1900,
        height: 1126,
        bgID: '#bgimg'
    });
	
