;
function getmap() {
			var var_location = new google.maps.LatLng(49.439050,7.764003);
  	  		var mapCanvas = document.getElementById("map");
  		  	var var_marker = new google.maps.Marker({
					position: var_location,
					title:"Autohaus am Stadtpark Holderbaum GmbH",
					 animation:google.maps.Animation.BOUNCE
					});

    	 	 var mapOptions = {
    	    	center: var_location, 
    	    	zoom: 19,
    	    	mapTypeId: google.maps.MapTypeId.ROADMAP,
    	    	scrollwheel: false
    	  	};

    	 	 var infowindow = new google.maps.InfoWindow({
    	 	    content: "Service mitten in Kaiserslautern<br>Auto am Stadtpark Holderbaum GmbH"
    	 	  });
    	 	 
    	 	 
    	 	 
       	  	var map = new google.maps.Map(mapCanvas, mapOptions);
       	  	var_marker.setMap(map);
       	  	infowindow.open(map,var_marker); 
    	}
